import { logApp } from './logger';
import { WebServer } from './web-server';

export default class App {
  webServer = new WebServer();

  constructor() {
    process
      .on('SIGTERM', async () => {
        logApp.info('Received SIGTERM');
        await this.shutdown(0);
      })
      .on('SIGINT', async () => {
        logApp.info('Received SIGINT');
        await this.shutdown(0);
      })
      .on('uncaughtException', (err) => {
        logApp.error('Uncaught exception');
        logApp.error(err.stack);
        logApp.end();
        process.exit(1);
      });
  }

  async init(): Promise<number | void> {
    const APP_START_TIME = process.hrtime();
    try {
      logApp.info('------');
      logApp.info('Starting application...');

      logApp.info('Initializing Web-server...');
      await this.webServer.initialize();
      logApp.info('Ok !');

      const ste = process.hrtime(APP_START_TIME);
      logApp.info(`Application started in ${ste[0] * 1000 + Math.round(ste[1] / 1000000)}ms, type CTRL + C to quit...`);
      return 0;
    } catch (e) {
      const err = e as Error;
      logApp.error(err.stack);
      logApp.end();
      process.exit(2);
    }
  }

  async shutdown(exitCode: number): Promise<void> {
    process.exitCode = exitCode;
    try {
      logApp.info('Shutting down...');

      logApp.info('Closing Web-server...');
      await this.webServer.close();
      logApp.info('Ok !');

    } catch (e) {
      const err = e as Error;
      logApp.error(err.stack);
      process.exitCode = 3;
    } finally {
      logApp.info(`Exiting process with code : ${process.exitCode}`);
      logApp.end();
      process.exit(process.exitCode);
    }
  }
}
