import { addQueryOptions, requiredBodyParams, checkAuth, checkRole, ROLES } from '@api/middlewares';
import { recipeController, userController } from '@api/controllers';

userController.collectionRouter
  .get('/', [addQueryOptions(userController.acceptedFields), userController.getAll])
  .post('/', [checkAuth, checkRole(ROLES.ADMIN), requiredBodyParams('username', 'password'), userController.createOne])
  .post('/authenticate', [requiredBodyParams('username', 'password'), userController.authenticate])
  ;

userController.ressourceRouter
  // /users/:id
  .get(`/`, [addQueryOptions(userController.acceptedFields), userController.getOne])
  .patch(`/`, [checkAuth, userController.patchOne])
  .delete(`/`, [checkAuth, userController.deleteOne])

  // /users/:id/recipes
  .get('/recipes', [addQueryOptions(recipeController.acceptedFields), userController.getRecipesByUser]) 
  ;

export const users = userController.collectionRouter;