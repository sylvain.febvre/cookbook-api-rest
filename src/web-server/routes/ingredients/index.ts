import { ingredientController } from '@api/controllers';
import { addQueryOptions, requiredBodyParams, checkRole, ROLES } from '@api/middlewares';


ingredientController.collectionRouter
  .get('/', [addQueryOptions(ingredientController.acceptedFields), ingredientController.getAll])
  .post('/', [checkRole(ROLES.ADMIN), requiredBodyParams('name'), ingredientController.createOne])
  ;

ingredientController.ressourceRouter
  .get('/', [addQueryOptions(ingredientController.acceptedFields), ingredientController.getOne])
  .patch(`/`, [checkRole(ROLES.ADMIN), ingredientController.patchOne])
  .delete(`/`, [checkRole(ROLES.ADMIN), ingredientController.deleteOne])
  ;

export const ingredients = ingredientController.collectionRouter;