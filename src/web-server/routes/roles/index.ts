import { addQueryOptions, checkRole, requiredBodyParams, ROLES } from '@api/middlewares';
import { roleController, userController } from '@api/controllers';

roleController.collectionRouter
  .get('/', [addQueryOptions(roleController.acceptedFields), roleController.getAll])
  .post('/', [checkRole(ROLES.ADMIN), requiredBodyParams('id', 'name', 'desc'), roleController.createOne])
  ;

roleController.ressourceRouter
  .get('/', [addQueryOptions(roleController.acceptedFields), roleController.getOne])
  .patch(`/`, [checkRole(ROLES.ADMIN), roleController.patchOne])
  .delete(`/`, [checkRole(ROLES.ADMIN), roleController.deleteOne])
  .get(`/users`, [addQueryOptions(userController.acceptedFields), roleController.getUsersByRole])
  ;

export const roles = roleController.collectionRouter;