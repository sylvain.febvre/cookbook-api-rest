import { checkAuth } from '@api/middlewares';
import { Router } from 'express';
import { ingredients } from './ingredients';
import { recipes } from './recipes';
import { roles } from './roles';
import { steps } from './steps';
import { users } from './users';


export const routes = Router();

routes
  .use('/users', users)

  // You need to be auth to access any routes
  .use('/roles', [checkAuth, roles])

  // You need to be auth to access any other route than get
  .post('*', checkAuth)
  .put('*', checkAuth)
  .delete('*', checkAuth)
  .patch('*', checkAuth)
  .use('/recipes', recipes)
  .use('/ingredients', ingredients)
  .use('/steps', steps)
  ;