import { ingredientController, recipeController, stepController } from '@api/controllers';
import { addQueryOptions, checkIfExists, requiredBodyParams } from '@api/middlewares';

const createOneParam = [
  'name',
  'difficulty',
  'prep_time',
  'cook_time',
];

recipeController.collectionRouter
  .get('/', [addQueryOptions(recipeController.acceptedFields), recipeController.getAll])
  .post('/', [requiredBodyParams(...createOneParam), recipeController.createOne])
  ;

recipeController.ressourceRouter
  // /recipes/:id
  .get(`/`, [addQueryOptions(recipeController.acceptedFields), recipeController.getOne])
  .patch(`/`, [recipeController.patchOne])
  .delete(`/`, [recipeController.deleteOne])
  // /recipes/:id/ingredients
  .get('/ingredients', [addQueryOptions({}), recipeController.getIngredients])
  // /recipes/:id/ingredients/:idIngredient
  .param(ingredientController.paramName, checkIfExists(ingredientController.model)) // Check if ingredient exists
  .get(`/ingredients/:${ingredientController.paramName}`, recipeController.getIngredient)
  .put(`/ingredients/:${ingredientController.paramName}`, [requiredBodyParams('quantity', 'unit_id'), recipeController.putIngredient])
  .delete(`/ingredients/:${ingredientController.paramName}`, recipeController.removeIngredient)
  // /recipes/:id/steps
  .get('/steps', [addQueryOptions(stepController.acceptedFields), recipeController.getSteps])
  // /recipes/:id/images
  .get('/images', [/* Query Options Images ,*/ recipeController.getImages])
  ;

export const recipes = recipeController.collectionRouter;