import { stepController } from '@api/controllers';
import { addQueryOptions, requiredBodyParams } from '@api/middlewares';


stepController.collectionRouter
  .get('/', [addQueryOptions(stepController.acceptedFields), stepController.getAll])
  .post('/', [requiredBodyParams('recipe_id', 'instruction'), stepController.createOne])
  ;

stepController.ressourceRouter
  .get('/', stepController.getOne)
  .patch('/', stepController.patchOne)
  .delete('/', stepController.deleteOne)
  ;

export const steps = stepController.collectionRouter;