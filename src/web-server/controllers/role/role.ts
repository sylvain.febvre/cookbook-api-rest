import { QueryAcceptedFields, RoleBody, RoleUpdateBody } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { Role } from '@database/models';
import { RequestHandler } from 'express';
import { CollectionController } from '../collection-controller';

class RoleController extends CollectionController<Role> {
  readonly acceptedFields: QueryAcceptedFields = {
    show: ['id', 'name', 'desc'],
    include: ['users'],
    sort: ['id', 'name'],
  };

  readonly createOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as RoleBody;
      const id = body.id,
        name = body.name.trim(),
        desc = body.desc.trim();

      if (name.length > 20)
        return next(getApiError(ApiErrorCodes.ROLE_NAME_TOO_LONG, { name }));

      if (desc.length > 255)
        return next(getApiError(ApiErrorCodes.ROLE_DESC_TOO_LONG, { description: desc }));

      // check if role already exists
      const role = await this.model.findByPk(id);
      if (role)
        return next(getApiError(ApiErrorCodes.ROLE_ALREADY_EXISTS, {
          id: role.id,
          name: role.name,
        }));

      const newRole = await this.model.create({
        id,
        name,
        desc,
      });
      await newRole.reload();

      res.setHeader('Location', `/roles/${newRole.id}`);
      return res.status(201).json(newRole);
    } catch (e) {
      return next(e);
    }
  }

  readonly patchOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as RoleUpdateBody;
      let { name, desc } = body;

      const role = await this.model.findByPk(req.params[this.paramName]) as Role;

      if (name) {
        name = name.trim();
        if (name.length > 20)
        return next(getApiError(ApiErrorCodes.ROLE_NAME_TOO_LONG, { name }));

        role.name = name;
      }

      if (desc) {
        desc = desc.trim();
        if (desc.length > 255)
        return next(getApiError(ApiErrorCodes.ROLE_DESC_TOO_LONG, { description: desc }));
        role.desc = desc;
      }

      await role.save();
      await role.reload();

      res.setHeader('Location', `/roles/${role.id}`);
      return res.json(role);
    } catch (e) {
      return next(e);
    }
  }

  readonly getUsersByRole: RequestHandler = async (req, res, next) => {
    try {
      const users = await (await this.model.findByPk(req.params[this.paramName]) as Role).getUsers(req.api.findOptions);

      if (!users || users.length <= 0) {
        return res.sendStatus(204);
      }

      return res.json(users);
    } catch (e) {
      return next(e);
    }
  }
}

export const roleController = new RoleController({
  paramName: 'roleId',
});