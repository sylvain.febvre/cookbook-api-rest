import { Role, User } from '@database/models';
import { Request, Response } from 'express';
import { roleController } from '@api/controllers';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { UserAttributes } from 'database/models/user.model';

describe('roleController handlers', () => {
  const mockRequest: Partial<Request> = {
    body: {},
    params: {
      roleId: '1'
    },
    api: {
      findOptions: {}
    }
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
    mockRequest.body = {};
    mockResponse.json = jest.fn();
    mockResponse.sendStatus = jest.fn();
    mockResponse.status = jest.fn().mockReturnThis();
    mockResponse.setHeader = jest.fn();
  });

  describe('createOne', () => {

    beforeEach(() => {
      mockRequest.body = {
        id: 1,
        name: 'un ',
        desc: ' one two three            ',
      };
    });

    describe('CREATED - 201', () => {
      test('send data', async () => {
        const expectedCreateAttributes = {
          id: 1,
          name: 'un',
          desc: 'one two three',
        };
        const newExpectedRole = Role.build(expectedCreateAttributes);
        newExpectedRole['reload'] = jest.fn().mockResolvedValueOnce(undefined);
        Role['findByPk'] = jest.fn().mockResolvedValueOnce(undefined);
        Role['create'] = jest.fn().mockResolvedValueOnce(newExpectedRole);
        await roleController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/roles/${newExpectedRole.id}`);
        expect(mockResponse.status).toBeCalledWith(201);
        expect(mockResponse.json).toBeCalledWith(newExpectedRole);
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('name > 20 characters', async () => {
        mockRequest.body = {
          id: 1,
          name: 'nameistoolonghshjdthe',
          desc: 'one two three',
        };
        await roleController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.ROLE_NAME_TOO_LONG,
          details: {
            name: mockRequest.body.name,
          }
        }));
      });

      test('desc > 255 characters', async () => {
        mockRequest.body = {
          id: 1,
          name: 'un',
          desc: `descthescdhatreshtrkduejdnvhfneidleritojsdhtbendsdescthescdhatreshtrkduejdnvhfneidleritojsdhtbends
          descthescdhatreshtrkduejdnvhfneidleritojsdhtbendsdescthescdhatreshtrkduejdnvhfneidleritojsdhtbends
          descthescdhatreshtrkduejdnvhfneidleritojsdhtbendsdescthescdhatreshtrkduejdnvhfneidleritojsdhtbends`,
        };
        await roleController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.ROLE_DESC_TOO_LONG,
          details: {
            description: mockRequest.body.desc,
          }
        }));
      });
    });

    describe('CONFLICT - 409', () => {
      test('role already exists', async () => {
        const alreadyExistsRoleAttributes = {
          id: 1,
          name: 'who',
          desc: 'care',
        };
        const alreadyExistsRole = Role.build(alreadyExistsRoleAttributes);
        Role['findByPk'] = jest.fn().mockResolvedValueOnce(alreadyExistsRole);
        await roleController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.ROLE_ALREADY_EXISTS,
          details: {
            id: alreadyExistsRoleAttributes.id,
            name: alreadyExistsRoleAttributes.name,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('genereic error', async () => {
        mockRequest.body = {
          id: 1,
          name: 'john',
          desc: 'one two three',
        };
        const error = new Error('createOne error');
        Role['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await roleController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('patchOne', () => {
    const inBaseRoleAttribute = {
      id: 1,
      name: 'old name',
      desc: 'old desc',
    };

    beforeEach(() => {
      const inBaseRole = Role.build(inBaseRoleAttribute);
      inBaseRole['save'] = jest.fn();
      inBaseRole['reload'] = jest.fn();
      Role['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseRole);
    });

    describe('OK - 200', () => {
      it('should update role', async () => {
        mockRequest.body = {
          name: 'new name    ',
          desc: '    new desc',
        };
        await roleController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/roles/1`);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          id: 1,
          name: 'new name',
          desc: 'new desc',
        }));
      });
    });




    describe('BAD_REQUEST - 400', () => {
      test('name > 20 characters', async () => {
        mockRequest.body = {
          name: 'new nameistoolonghshjdthe',
          desc: 'new desc',
        };
        await roleController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.ROLE_NAME_TOO_LONG,
          details: {
            name: mockRequest.body.name,
          }
        }));
      });

      test('desc > 255 characters', async () => {
        mockRequest.body = {
          name: 'new name',
          desc: `new descthescdhatreshtrkduejdnvhfneidleritojsdhtbendsdescthescdhatreshtrkduejdnvhfneidleritojsdhtbends
          descthescdhatreshtrkduejdnvhfneidleritojsdhtbendsdescthescdhatreshtrkduejdnvhfneidleritojsdhtbends
          descthescdhatreshtrkduejdnvhfneidleritojsdhtbendsdescthescdhatreshtrkduejdnvhfneidleritojsdhtbends`,
        };
        await roleController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.ROLE_DESC_TOO_LONG,
          details: {
            description: mockRequest.body.desc,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('patchOne error');
        Role['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await roleController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
    
  });

  describe('getUsersByRole', () => {
    const inBaseRoleAttribute = mockRequest.body = {
      id: 1,
      name: 'name',
      desc: 'desc',
    };
    let inBaseRole: Role;
    const inBaseUseraAssociation = [
      User.build({
        id: 1,
        username: 'user1',
        role_id: 1
      } as UserAttributes),
      User.build({
        id: 2,
        username: 'user2',
        role_id: 1
      } as UserAttributes)
    ];

    beforeEach(() => {
      inBaseRole = Role.build(inBaseRoleAttribute);
      inBaseRole['getUsers'] = jest.fn().mockResolvedValueOnce(inBaseUseraAssociation);
      Role['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseRole);
    });

    describe('OK - 200', () => {
      test('Send users role', async () => {
        await roleController.getUsersByRole(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(inBaseUseraAssociation);
      });
    });

    describe('NO_CONTENT - 204', () => {
      test('no users', async () => {
        inBaseRole['getUsers'] = jest.fn().mockResolvedValueOnce([]);
        await roleController.getUsersByRole(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(204);
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('getUsersByRole error');
        Role['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await roleController.getUsersByRole(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

});