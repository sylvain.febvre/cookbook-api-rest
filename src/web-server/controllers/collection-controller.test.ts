import { QueryAcceptedFields } from '@api/utils';
import { RequestHandler } from 'express';
import { DataTypes, Model } from 'sequelize';
import { CollectionController } from './collection-controller';
import { Request, Response } from 'express';
import { sequelize } from '@database';

interface MockAttributes {
  id: number;
  name: string;
}

class Mock extends Model<MockAttributes>
  implements MockAttributes {

  id!: number;
  name!: string;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;
}

Mock.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize,
  tableName: 'mock',
});

describe('CollectionController class', () => {
  /**
   * mock implementation of CollectionCOntroller
   */
  class MockController extends CollectionController<Model> {
    readonly acceptedFields: QueryAcceptedFields = {};
    readonly createOne: RequestHandler = (req, res) => res.send('mock createOne');
    readonly patchOne: RequestHandler = (req, res) => res.send('mock patchOne');
  }

  let mockController: MockController | undefined;

  beforeEach(() => {
    jest.restoreAllMocks();
    mockController = undefined;
  });

  describe('Class instanciation', () => {
    it('should instanciate without parameters', () => {
      mockController = new MockController();
      expect(mockController).toBeInstanceOf(MockController);
      expect(mockController.model).toEqual(Mock);
      expect(mockController.paramName).toMatch('MockControllerId');
    });

    it('should instanciate with parameters', () => {
      mockController = new MockController({
        modelName: 'Mock',
        paramName: 'mockId',
      });
      expect(mockController).toBeInstanceOf(MockController);
      expect(mockController.model).toEqual(Mock);
      expect(mockController.paramName).toMatch('mockId');
    });

    it('should throw error if model not found in sequelize', () => {
      expect.assertions(1);
      try {
        new MockController({ modelName: 'BadModelName' });
      } catch (error) {
        expect((error as Error).message).toMatch(`Model BadModelName doesn't exist in sequelize`);
      }
    });

    it('should throw error if modelName cannot be infered', () => {
      class badclassname extends MockController {}
      expect.assertions(1);
      try {
        new badclassname();
      } catch(error) {
        expect((error as Error).message).toMatch(`Can't infer model name from class name badclassname`);
      }
    });

    it('should throw error if model infered is not found', () => {
      class NomodelController extends MockController {}
      expect.assertions(1);
      try {
        new NomodelController();
      } catch (error) {
          expect((error as Error).message).toMatch(`Model Nomodel infered from class name doesn't exist in sequelize`);
      }
    });
  });

  describe('Request handler', () => {
    const mockRequest: Partial<Request> = {
      body: {},
      params: {
        mockId: '1'
      },
      api: {
        findOptions: {}
      }
    };
    const mockResponse: Partial<Response> = {
      sendStatus: jest.fn(),
      json: jest.fn(),
      setHeader: jest.fn(),
    };
    const nextFunction = jest.fn();

    beforeEach(() => {
      nextFunction.mockRestore();
      mockController = new MockController({
        modelName: 'Mock',
        paramName: 'mockId',
      });
    });

    describe('getAll', () => {
      it('should send data with status 200', async () => {
        const expectedResult = [
          Mock.build({
            id: 1,
            name: 'un',
          }),
          Mock.build({
            id: 2,
            name: 'deux',
          })
        ];
        Mock['findAll'] = jest.fn().mockResolvedValueOnce(expectedResult);
        Mock['count'] = jest.fn().mockResolvedValueOnce(expectedResult.length);
        await mockController?.getAll(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(expectedResult);
        expect(mockResponse.setHeader).toHaveBeenNthCalledWith(2, 'X-Total-Count', expectedResult.length);
      });

      it('should send status 204', async () => {
        Mock['findAll'] = jest.fn().mockResolvedValueOnce([]);
        await mockController?.getAll(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(204);
      });

      it('should call next on error', async () => {
        const error = new Error('getAll error');
        Mock['findAll'] = jest.fn().mockRejectedValueOnce(error);
        await mockController?.getAll(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });

    describe('getOne', () => {
      it('should send data with status 200', async () => {
        const expectedResult = Mock.build({
          id: 1,
          name: 'un',
        });
        Mock['findByPk'] = jest.fn().mockResolvedValueOnce(expectedResult);
        await mockController?.getOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(expectedResult);
      });

      it('should call next on error', async () => {
        const error = new Error('getOne error');
        Mock['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await mockController?.getOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });

    describe('deleteOne', () => {
      it('should send status 200', async () => {
        Mock['destroy'] = jest.fn().mockResolvedValueOnce(1);
        await mockController?.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(200);  
      });

      it('should call next on error', async () => {
        const error = new Error('deleteOne error');
        Mock['destroy'] = jest.fn().mockRejectedValueOnce(error);
        await mockController?.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });
  });
});