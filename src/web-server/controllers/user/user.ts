import { JWTPayload, QueryAcceptedFields, UserUpdateBody, UserBody } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { JWT, User } from '@database/models';
import { randomBytes, createHmac } from 'crypto';
import { RequestHandler } from 'express';
import { Sequelize } from 'sequelize';
import { CollectionController } from '../collection-controller';
import { sign } from 'jsonwebtoken';
import { ROLES } from '@api/middlewares';

class UserController extends CollectionController<User> {
  readonly acceptedFields: QueryAcceptedFields = {
    show: ['id', 'username', 'role_id'],
    include: ['role'],
    sort: ['id', 'username', 'role_id'],
  }

  readonly createOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as UserBody;
      const username = body.username.trim(),
        password = body.password.trim();

      if (username.length < 3)
        return next(getApiError(ApiErrorCodes.USER_NAME_TOO_SHORT, { username }));

      if (password.length < 8)
        return next(getApiError(ApiErrorCodes.USER_PASSWORD_TOO_SHORT));

      const userExists = await this.model.findOne({
        where: Sequelize.where(
          Sequelize.fn('lower', Sequelize.col('username')), username.toLowerCase()
        )
      });
      if (userExists)
        return next(getApiError(ApiErrorCodes.USER_NAME_ALREADY_EXISTS, { username }));

      const salt = randomBytes(Math.ceil(12 / 2)).toString('hex').slice(0, 12);
      const pwdHash = createHmac('sha512', salt).update(password).digest('hex');

      const newUser = await this.model.create({
        username,
        password: pwdHash,
        salt,
      });

      await newUser.reload();

      res.setHeader('Location', `/users/${newUser.id}`);
      return res.status(201).json(newUser);
    } catch (e) {
      return next(e);
    }
  }

  readonly authenticate: RequestHandler = async (req, res, next) => {
    try {
      const { username, password } = req.body as UserBody;

      const user = await this.model.findOne({
        where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('username')), username.toLowerCase()),
        attributes: {
          include: ['password', 'salt', 'role_id']
        }
      });

      if (!user) {
        return next(getApiError(ApiErrorCodes.USER_NOT_FOUND, { username }));
      }

      if (createHmac('sha512', user.salt).update(password).digest('hex') !== user.password) {
        return next(getApiError(ApiErrorCodes.USER_WRONG_PASSWORD));
      }

      const payload: Partial<JWTPayload> = {
        id: user.id,
        role: user.role_id,
        username: user.username,
      };

      const secret = process.env.JWT_SECRET;
      const expiresIn = '15d';

      const token = sign(payload, secret, {
        expiresIn
      });

      // Add token hash in db
      const tokenHash = createHmac('sha512', 'token').update(token).digest('hex');
      JWT.create({
        hash: tokenHash,
        user_id: user.id,
      });

      return res.json({
        access_token: token,
        token_type: 'JWT',
        expiresIn,
      });

    } catch (e) {
      return next(e);
    }
  }

  readonly patchOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as UserUpdateBody;
      const newPassword = body.newPassword?.trim();
      const username = body.username?.trim();

      // Check if user is auth
      if (!req.user)
        return next(getApiError(ApiErrorCodes.NOT_SIGNED));

      // Not self and not admin
      if (req.user.id !== parseInt(req.params[this.paramName], 10) && req.user.role !== ROLES.ADMIN)
        return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, { ressource: 'user' }));

      const user = await this.model.findByPk(req.params[this.paramName], {
        attributes: {
          include: ['password', 'salt']
        }
      }) as User;

      // Check for role_id (must be admin)
      if (typeof body.role_id !== 'undefined' && req.user.role !== ROLES.ADMIN)
        return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, {
          ressource: 'user',
          field: 'role_id',
        }));

      // No / wrong password and not admin
      if ((!body.password || body.password && createHmac('sha512', user.salt).update(body.password).digest('hex') !== user.password) && req.user.role !== ROLES.ADMIN)
        return next(getApiError(ApiErrorCodes.USER_WRONG_PASSWORD));

      if (username && username.length < 3)
        return next(getApiError(ApiErrorCodes.USER_NAME_TOO_SHORT, { username }));

      if (newPassword) {
        // Only self can update password even if admin
        if (req.user.id !== parseInt(req.params[this.paramName], 10))
          return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, {
            ressource: 'user',
            field: 'password',
          }));

        if (newPassword.length < 8)
          return next(getApiError(ApiErrorCodes.USER_PASSWORD_TOO_SHORT));
      }

      if (typeof body.role_id !== 'undefined') {
        user.role_id = body.role_id;
      }

      if (username) {
        // check if username already exists
        const userExists = await this.model.findOne({
          where: Sequelize.where(
            Sequelize.fn('lower', Sequelize.col('username')), username.toLowerCase()
          )
        });
        if (userExists)
          return next(getApiError(ApiErrorCodes.USER_NAME_ALREADY_EXISTS, { username }));
        user.username = username;
      }

      if (newPassword) {
        const salt = randomBytes(Math.ceil(12 / 2)).toString('hex').slice(0, 12);
        const pwdHash = createHmac('sha512', salt).update(newPassword).digest('hex');
        user.password = pwdHash;
        user.salt = salt;
      }
      await user.save();
      await user.reload();

      // User has been modified, remove all JWT token associated
      await JWT.destroy({
        where: {
          user_id: user.id,
        }
      });

      res.setHeader('Location', `/users/${user.id}`);
      return res.json(user);
    } catch (e) {
      return next(e);
    }
  }

  readonly deleteOne: RequestHandler = async (req, res, next) => {
    try {

      const payload = req.user as JWTPayload;

      // Only self can delete
      if (payload.id !== parseInt(req.params[this.paramName], 10))
        return next(getApiError(ApiErrorCodes.FORBIDDEN_DELETE, { ressource: 'user' }));

      await this.model.destroy({
        where: { id: req.params[this.paramName] },
        limit: 1,
      });
      return res.sendStatus(200);
    } catch (e) {
      return next(e);
    }
  }

  readonly getRecipesByUser: RequestHandler = async (req, res, next) => {
    try {
      const recipes = await (await this.model.findByPk(req.params[this.paramName]) as User).getRecipes(req.api.findOptions);

      if (!recipes || recipes.length <= 0) {
        return res.sendStatus(204);
      }

      return res.json(recipes);
    } catch (e) {
      return next(e);
    }
  }
}

export const userController = new UserController({
  paramName: 'userId',
});