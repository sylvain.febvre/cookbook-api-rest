import { JWT, Recipe, User } from '@database/models';
import { Request, Response } from 'express';
import { userController } from '@api/controllers';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { JWTPayload, UserBody, UserUpdateBody } from '@api/utils';
import { UserAttributes } from 'database/models/user.model';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import { RecipeAttributes } from 'database/models/recipe.model';

describe('userController handlers', () => {
  const mockRequest: Partial<Request> = {
    body: {},
    params: {
      userId: '1',
    },
    api: {
      findOptions: {}
    },
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
    mockRequest.body = {};
    mockResponse.json = jest.fn();
    mockResponse.sendStatus = jest.fn();
    mockResponse.status = jest.fn().mockReturnThis();
    mockResponse.setHeader = jest.fn();
  });

  describe('createOne', () => {

    beforeEach(() => {
      mockRequest.body = {
        password: 'password    ',
        username: '    username',
      } as UserBody;
      User['findOne'] = jest.fn();
      User['create'] = jest.fn();
    });

    describe('CREATED - 201', () => {
      test('send user data', async () => {
        const expectedCreateAttributes = {
          id: 1,
          username: 'username',
          password: 'password',
        } as UserAttributes;
        const newExpectedUser = User.build(expectedCreateAttributes);
        newExpectedUser['reload'] = jest.fn().mockResolvedValueOnce(undefined);
        User['findOne'] = jest.fn().mockResolvedValueOnce(undefined);
        User['create'] = jest.fn().mockResolvedValueOnce(newExpectedUser);
        await userController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/users/${newExpectedUser.id}`);
        expect(mockResponse.status).toBeCalledWith(201);
        expect(mockResponse.json).toBeCalledWith(newExpectedUser);
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('username < 3 characters', async () => {
        mockRequest.body = {
          username: 'uu',
          password: 'password',
        } as UserBody;
        await userController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_NAME_TOO_SHORT,
          details: {
            username: mockRequest.body.username,
          }
        }));
      });

      test('password < 8 characters', async () => {
        mockRequest.body = {
          username: 'username',
          password: 'short',
        } as UserBody;
        await userController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_PASSWORD_TOO_SHORT,
        }));
      });
    });

    describe('CONFLICT - 409', () => {
      test('user already exists', async () => {
        const alreadyExistsUserAttributes = {
          id: 12,
          username: 'username',
          password: 'password',
        } as UserAttributes;
        const alreadyExistsRole = User.build(alreadyExistsUserAttributes);
        User['findOne'] = jest.fn().mockResolvedValueOnce(alreadyExistsRole);
        await userController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_NAME_ALREADY_EXISTS,
          details: {
            username: alreadyExistsUserAttributes.username,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should send Error on error', async () => {
        const error = new Error('createOne error');
        User['findOne'] = jest.fn().mockRejectedValueOnce(error);
        await userController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('authenticate', () => {
    const plainPassword = 'password';
    const salt = 'xc24f5g8re45';
    const hash = crypto.createHmac('sha512', salt).update(plainPassword).digest('hex');
    const userAttribute = {
      id: 1,
      password: hash,
      salt: salt,
      role_id: 99,
      username: 'username',
    } as UserAttributes;

    beforeEach(() => {
      User['findOne'] = jest.fn().mockResolvedValueOnce(User.build(userAttribute));
      JWT['create'] = jest.fn();
    });

    describe('OK - 200', () => {
      it('should authenticate user and send token with status 200', async () => {
        mockRequest.body = {
          username: 'username',
          password: 'password',
        } as UserBody;
        const payload: Partial<JWTPayload> = {
          id: 1,
          role: 99,
          username: 'username',
        };
        const expectedToken = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '15d' });
        await userController.authenticate(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith({
          access_token: expectedToken,
          token_type: 'JWT',
          expiresIn: '15d',
        });
      });
    });

    describe('UNAUTHORIZED - 401', () => {
      test('user not found', async () => {
        mockRequest.body = {
          password: 'password',
          username: 'notFound',
        } as UserBody;
        User['findOne'] = jest.fn().mockResolvedValueOnce(undefined);
        await userController.authenticate(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_NOT_FOUND,
        }));
      });

      test('wrong password', async () => {
        mockRequest.body = {
          password: 'wrongPass',
          username: 'username',
        } as UserBody;
        await userController.authenticate(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_WRONG_PASSWORD,
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should call next on any error', async () => {
        mockRequest.body = {
          username: 'username',
          password: 'password',
        } as UserBody;
        const error = new Error('authenticate error');
        User['findOne'] = jest.fn().mockRejectedValueOnce(error);
        await userController.authenticate(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('patchOne', () => {
    const basePlainPassword = 'inBasePassword';
    const baseSalt = 'xc24f5g8re45';
    const baseHash = crypto.createHmac('sha512', baseSalt).update(basePlainPassword).digest('hex');
    const inBaseUserAttribute = {
      id: 1,
      password: baseHash,
      salt: baseSalt,
      role_id: 99,
      username: 'inBaseUsername',
    } as UserAttributes;

    beforeEach(() => {
      crypto.randomBytes = jest.fn().mockReturnValue('ab12cd34ef56');
      const inBaseUser = User.build(inBaseUserAttribute);
      inBaseUser['save'] = jest.fn();
      inBaseUser['reload'] = jest.fn();
      User['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseUser);
      User['findOne'] = jest.fn().mockResolvedValueOnce(undefined);
      JWT['destroy'] = jest.fn();
      mockRequest.user = {
        id: 1,
        role: 99,
        username: 'username',
      };
    });

    describe('OK - 200', () => {
      test('updated user', async () => {
        mockRequest.body = {
          newPassword: 'newPassword',
          username: 'newUsername',
          password: basePlainPassword,
        } as UserUpdateBody;
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/users/1`);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          id: 1,
          role_id: 99,
          username: 'newUsername'
        } as UserAttributes));
      });

      test('updated user with new role', async () => {
        mockRequest.body = {
          role_id: 12,
        } as UserUpdateBody;
        mockRequest.user = {
          id: 15,
          role: 0,
          username: 'admin',
        };
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/users/1`);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          id: 1,
          role_id: 12,
          username: 'inBaseUsername'
        } as UserAttributes));
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('username > 3', async () => {
        mockRequest.body = {
          username: 'az',
          password: basePlainPassword,
        } as UserUpdateBody;
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_NAME_TOO_SHORT,
          details: {
            username: mockRequest.body.username,
          }
        }));
      });

      test('password > 8', async () => {
        mockRequest.body = {
          newPassword: 'short',
          password: basePlainPassword,
        } as UserUpdateBody;
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_PASSWORD_TOO_SHORT,
        }));
      });

    });

    describe('UNAUTHORIZED - 401', () => {
      test('user is not logged', async () => {
        mockRequest.user = undefined;
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.NOT_SIGNED,
        }));
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('user do not have permission to update', async () => {
        mockRequest.user = {
          id: 12,
          role: 99,
          username: 'auser'
        };
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'user',
          }
        }));
      });

      test('wrong user password and not admin', async () => {
        mockRequest.body = {
          password: 'WrOnG PassW0rD',
        } as UserUpdateBody;
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_WRONG_PASSWORD,
        }));
      });

      test('user try to update role and is not admin', async () => {
        mockRequest.body = {
          role_id: 0,
        };
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'user',
            field: 'role_id',
          }
        }));
      });

      test('user trying to change password is not himself', async () => {
        mockRequest.body = {
          newPassword: 'newPassword',
        };
        mockRequest.user = {
          id: 12,
          role: 0,
          username: 'admin'
        };
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'user',
            field: 'password',
          }
        }));
      });
    });

    describe('CONFLICT - 409', () => {
      test('username already exists', async () => {
        User['findOne'] = jest.fn().mockResolvedValueOnce(User.build({
          username: 'usernameUsed',
          id: 2,
          role_id: 99,
        } as UserAttributes));
        mockRequest.body = {
          username: 'usernameUsed',
          password: basePlainPassword,
        } as UserUpdateBody;
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.USER_NAME_ALREADY_EXISTS,
          details: {
            username: mockRequest.body.username,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should call next on any error', async () => {
        const error = new Error('patchOne error');
        User['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await userController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('deleteOne', () => {
    beforeEach(() => {
      mockRequest.user = {
        id: 1,
        role: 99,
        username: 'username',
      };
      User['destroy'] = jest.fn();
    });

    describe('OK - 200', () => {
      test('delete', async () => {
        User['destroy'] = jest.fn().mockResolvedValueOnce(1);
        await userController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(200);
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('another user try to delete', async () => {
        mockRequest.user = {
          id: 5,
          username: 'username',
          role: 99,
        };
        await userController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_DELETE,
          details: {
            ressource: 'user',
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should call next on error', async () => {
        const error = new Error('deleteOne error');
        User['destroy'] = jest.fn().mockRejectedValueOnce(error);
        await userController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });

  });

  describe('getRecipesByUser', () => {
    const inBaseUserAttribute = mockRequest.body = {
      id: 1,
      username: 'username',
      password: 'password',
    } as UserAttributes;
    let inBaseUser: User;
    const inBaseRecipeAssociation: Recipe[] = [
      Recipe.build({
        id: 1,
        user_id: 1,
        name: 'a recipe',
      } as RecipeAttributes),
      Recipe.build({
        id: 2,
        user_id: 1,
        name: 'a 2nd recipe',
      } as RecipeAttributes),
    ];

    beforeEach(() => {
      inBaseUser = User.build(inBaseUserAttribute);
      inBaseUser['getRecipes'] = jest.fn().mockResolvedValueOnce(inBaseRecipeAssociation);
      User['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseUser);
    });

    describe('OK - 200', () => {
      test('send recipes', async () => {
        await userController.getRecipesByUser(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(inBaseRecipeAssociation);
      });
    });

    describe('NO_CONTENT - 204', () => {
      test('no recipe', async () => {
        inBaseUser['getRecipes'] = jest.fn().mockResolvedValueOnce([]);
        await userController.getRecipesByUser(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(204);
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should send Error on error', async () => {
        const error = new Error('getRecipesByUser error');
        User['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await userController.getRecipesByUser(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });

  });
});