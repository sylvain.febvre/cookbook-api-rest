import { ROLES } from '@api/middlewares';
import { JWTPayload, QueryAcceptedFields, RecipeBody, RecipeUpdateBody } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { Recipe, Quantity } from '@database/models';
import { RequestHandler } from 'express';
import { CollectionController } from '../collection-controller';
import { ingredientController } from '../ingredient/ingredient';

class RecipeController extends CollectionController<Recipe> {
  readonly acceptedFields: QueryAcceptedFields = {
    show: ['id', 'name', 'difficulty', 'prep_time', 'rest_time', 'cook_time', 'desc', 'note', 'persons', 'user_id'],
    include: ['user', 'steps'],
    sort: ['id', 'name', 'difficulty', 'prep_time', 'rest_time', 'cook_time', 'persons', 'user_id'],
  };

  /**
   * Create a new recipe
   */
  readonly createOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as RecipeBody;
      const payload = req.user as JWTPayload;

      // Check required params [name, difficulty, prep_time, cook_time]
      // name
      const name = body.name.trim();
      if (name.length > 64) {
        return next(getApiError(ApiErrorCodes.RECIPE_NAME_TOO_LONG, { name }));
      }

      // difficulty
      const difficulty = body.difficulty;
      if (![1, 2, 3].includes(difficulty)) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_DIFFICULTY, { difficulty }));
      }

      // prep_time
      const prep_time = parseInt(body.prep_time as unknown as string, 10);
      if (isNaN(prep_time) || prep_time < 0) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_PREP_TIME, { prep_time }));
      }

      // cook_time
      const cook_time = parseInt(body.cook_time as unknown as string, 10);
      if (isNaN(cook_time) || cook_time < 0) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_COOK_TIME, { cook_time }));
      }

      // Check optional properties [rest_time, desc, note, persons]
      // rest_time
      const rest_time = body.rest_time ? parseInt(body.rest_time as unknown as string, 10) : undefined;
      if (rest_time !== undefined && (isNaN(rest_time) || rest_time < 0)) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_REST_TIME, { rest_time }));
      }

      // desc
      const desc = body.desc?.trim();
      if (desc && desc.length > 255) {
        return next(getApiError(ApiErrorCodes.RECIPE_DESC_TOO_LONG, { description: desc }));
      }

      // note
      const note = body.note?.trim();
      if (note && note.length > 255) {
        return next(getApiError(ApiErrorCodes.RECIPE_NOTE_TOO_LONG, { note }));
      }

      // persons
      const persons = body.persons ? parseInt(body.persons as unknown as string, 10) : undefined;
      if (persons !== undefined && (isNaN(persons) || persons < 0)) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_PERSONS, { persons }));
      }


      const newRecipe = await this.model.create({
        name,
        difficulty,
        cook_time,
        prep_time,
        user_id: payload.id,
        ...(desc && { desc }),
        ...(note && { note }),
        ...(persons && { persons }),
        ...(rest_time && { rest_time }),
      });
      await newRecipe.reload();

      res.setHeader('Location', `/recipes/${newRecipe.id}`);
      return res.status(201).json(newRecipe);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Update a recipe
   */
  readonly patchOne: RequestHandler = async (req, res, next) => {
    const payload = req.user as JWTPayload;
    const { cook_time, desc, difficulty, name, note, persons, prep_time, rest_time } = req.body as RecipeUpdateBody;

    const recipe = await this.model.findByPk(req.params[this.paramName]) as Recipe;

    // Check user own recipe or is admin
    if (recipe.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
      return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, { ressource: 'recipe' }));
    }

    // Check params [name, difficulty, prep_time, cook_time, desc, note, persons]
    // name
    if (name) {
      const tname = name.trim();
      if (tname.length > 64)
        return next(getApiError(ApiErrorCodes.RECIPE_NAME_TOO_LONG, { name }));
      recipe.name = tname;
    }

    // difficulty
    if (difficulty !== undefined) {
      if (![1, 2, 3].includes(difficulty))
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_DIFFICULTY, { difficulty }));
      recipe.difficulty = difficulty;
    }

    // prep_time
    if (prep_time !== undefined) {
      const pprep_time = parseInt(prep_time as unknown as string, 10);
      if (isNaN(pprep_time) || pprep_time < 0)
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_PREP_TIME, { prep_time }));
      recipe.prep_time = pprep_time;
    }

    // rest_time
    if (rest_time !== undefined) {
      const prest_time = parseInt(rest_time as unknown as string, 10);
      if ((isNaN(prest_time) || prest_time < 0))
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_REST_TIME, { rest_time }));
      recipe.rest_time = prest_time;
    }

    // cook_time
    if (cook_time !== undefined) {
      const pcook_time = parseInt(cook_time as unknown as string, 10);
      if (isNaN(pcook_time) || pcook_time < 0)
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_COOK_TIME, { cook_time }));
      recipe.cook_time = pcook_time;
    }

    // desc
    if (desc !== undefined) {
      const tdesc = desc.trim();
      if (tdesc.length > 255)
        return next(getApiError(ApiErrorCodes.RECIPE_DESC_TOO_LONG, { description: desc }));
      recipe.desc = tdesc;
    }

    // note
    if (note !== undefined) {
      const tnote = note.trim();
      if (tnote.length > 255)
        return next(getApiError(ApiErrorCodes.RECIPE_NOTE_TOO_LONG, { note }));
      recipe.note = tnote;
    }

    // persons
    if (persons !== undefined) {
      const ppersons = parseInt(persons as unknown as string, 10);
      if ((isNaN(ppersons) || ppersons < 0))
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_PERSONS, { persons }));
      recipe.persons = ppersons;
    }

    await recipe.save();
    await recipe.reload();

    res.setHeader('Location', `/recipes/${recipe.id}`);
    return res.json(recipe);
  }

  /**
   * Delete a recipe
   */
  readonly deleteOne: RequestHandler = async (req, res, next) => {
    try {
      const payload = req.user as JWTPayload;

      const recipe = await this.model.findByPk(req.params[this.paramName]) as Recipe;

      // Check user own recipe or is admin
      if (recipe.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
        return next(getApiError(ApiErrorCodes.FORBIDDEN_DELETE, { ressource: 'recipe' }));
      }

      await recipe.destroy();

      return res.sendStatus(200);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Get ingredients of the recipe with quantity
   */
  readonly getIngredients: RequestHandler = async (req, res, next) => {
    try {
      const ingredients = await Quantity.findAll({
        limit: req.api.findOptions.limit,
        offset: req.api.findOptions.offset,
        where: {
          recipe_id: req.params[this.paramName]
        },
      });

      return res.json(ingredients);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Add or Update a quantity for an ingredient
   */
  readonly putIngredient: RequestHandler = async (req, res, next) => {
    try {
      const payload = req.user as JWTPayload;
      const { quantity, unit_id } = req.body;

      const recipe = await this.model.findByPk(req.params[this.paramName], {
        attributes: ['id'],
      }) as Recipe;

      const ingredientId = parseInt(req.params[ingredientController.paramName]);

      // Check user own recipe or is admin
      if (recipe.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
        return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, { ressource: 'recipe' }));
      }

      // quantity
      const parsedQuantity = parseInt(quantity as unknown as string, 10);
      if (isNaN(parsedQuantity) || parsedQuantity < 0) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_QUANTITY, { quantity }));
      }

      // unit_id
      const parsedUnitId = parseInt(unit_id as unknown as string, 10);
      if (isNaN(parsedUnitId) || parsedUnitId < 0) {
        return next(getApiError(ApiErrorCodes.RECIPE_WRONG_UNIT_ID, { unit_id }));
      }

      const [newQuantity] = await Quantity.upsert({
        ingredient_id: ingredientId,
        recipe_id: recipe.id,
        unit_id: parsedUnitId,
        quantity: parsedQuantity,
      });

      await newQuantity.reload();

      res.setHeader('Location', `/recipes/${recipe.id}/ingredients/${ingredientId}`);
      return res.json(newQuantity);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Get a recipe ingredient with quantity
   */
  readonly getIngredient: RequestHandler = async (req, res, next) => {
    try {
      const recipe_id = req.params[this.paramName];
      const ingredient_id = req.params[ingredientController.paramName];

      const ingredients = await Quantity.findOne({
        where: {
          recipe_id,
          ingredient_id,
        },
        include: ['ingredient']
      });

      if (!ingredients) {
        return next(getApiError(ApiErrorCodes.RECIPE_INGREDIENT_NOT_IN, {
          ingredient_id: parseInt(ingredient_id),
          recipe_id: parseInt(recipe_id),
        }));
      }

      return res.json(ingredients);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Remove an ingredient from a recipe
   */
  readonly removeIngredient: RequestHandler = async (req, res, next) => {
    try {
      const payload = req.user as JWTPayload;

      const ingredientId = parseInt(req.params[ingredientController.paramName]);

      const recipe = await this.model.findByPk(req.params[this.paramName], {
        attributes: ['id'],
      }) as Recipe;

      // Check user own recipe or is admin
      if (recipe.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
        return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, { ressource: 'recipe' }));
      }

      const quantity = await Quantity.findOne({
        attributes: {
          include: ['ingredient_id', 'recipe_id']
        },
        where: {
          recipe_id: recipe.id,
          ingredient_id: ingredientId,
        }
      });

      // Check if quantity exists
      if (!quantity) {
        return next(getApiError(ApiErrorCodes.RECIPE_INGREDIENT_NOT_IN, {
          ingredient_id: ingredientId,
          recipe_id: recipe.id,
        }));
      }

      await quantity.destroy();

      res.sendStatus(200);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Get all steps of a recipe
   */
  readonly getSteps: RequestHandler = async (req, res, next) => {
    try {
      const steps = await (await this.model.findByPk(req.params[this.paramName]) as Recipe).getSteps(req.api.findOptions);

      if (!steps || steps.length <= 0) {
        return res.sendStatus(204);
      }

      return res.json(steps);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * get all images of a recipe
   */
  readonly getImages: RequestHandler = async (req, res, next) => {
    return next(getApiError(ApiErrorCodes.NOT_IMPLEMENTED, { route: new URL(req.originalUrl, `${req.protocol}://${req.hostname}`).pathname }));
  }
}

export const recipeController = new RecipeController({
  paramName: 'recipeId',
});