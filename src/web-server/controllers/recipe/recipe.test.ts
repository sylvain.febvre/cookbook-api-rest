import { JWTPayload, RecipeBody, RecipeUpdateBody } from '@api/utils';
import { ApiErrorCodes, ApiError } from '@api/utils/error';
import { Ingredient, Quantity, QuantityUnit, Step } from '@database/models';
import { Recipe, RecipeAttributes } from 'database/models/recipe.model';
import { StepAttributes } from 'database/models/step.model';
import { Request, Response } from 'express';
import { recipeController } from './recipe';

describe('recipeController handlers', () => {
  const mockRequest: Partial<Request> = {
    body: {},
    params: {
      recipeId: '1',
      ingredientId: '2',
    },
    api: {
      findOptions: {}
    },
    user: {
      id: 12,
      role: 99,
      username: 'username'
    }
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
    mockRequest.body = {};
    mockResponse.json = jest.fn();
    mockResponse.sendStatus = jest.fn();
    mockResponse.status = jest.fn().mockReturnThis();
    mockResponse.setHeader = jest.fn();
  });

  describe('createOne', () => {

    beforeEach(() => {
      mockRequest.body = {
        name: 'A recipe',
        difficulty: 1,
        prep_time: 30,
        cook_time: 20,
      } as RecipeBody;
    });

    describe('CREATED - 201', () => {
      test('recipe created', async () => {
        const expectedCreateAttributes = {
          user_id: 12,
          name: 'A recipe',
          difficulty: 1,
          prep_time: 30,
          cook_time: 20,
          id: 1,
        } as RecipeAttributes;
        const newExpectedRecipe = Recipe.build(expectedCreateAttributes);
        newExpectedRecipe['reload'] = jest.fn().mockResolvedValue(undefined);
        Recipe['create'] = jest.fn().mockResolvedValue(newExpectedRecipe);
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body = {
          ...mockRequest.body,
          desc: 'A description',
          note: 'A note',
          persons: 2,
          rest_time: 20,
        } as RecipeBody;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/recipes/${newExpectedRecipe.id}`);
        expect(mockResponse.setHeader).toBeCalledTimes(2);
        expect(mockResponse.status).toBeCalledWith(201);
        expect(mockResponse.status).toBeCalledTimes(2);
        expect(mockResponse.json).toBeCalledWith(newExpectedRecipe);
        expect(mockResponse.json).toBeCalledTimes(2);
      });
    });

    describe('BAD_REQUEST - 401', () => {
      test('name > 64', async () => {
        const longName = [...Array(65)].map(() => 'a').join('');
        mockRequest.body.name = longName;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_NAME_TOO_LONG,
          details: {
            name: longName,
          }
        }));
      });

      test('desc and > 255', async () => {
        const longDesc = [...Array(256)].map(() => 'a').join('');
        mockRequest.body.desc = longDesc;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_DESC_TOO_LONG,
          details: {
            description: longDesc,
          }
        }));
      });

      test('difficulty is not 1, 2, 3', async () => {
        mockRequest.body.difficulty = 'NaN';
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_DIFFICULTY,
          details: {
            difficulty: mockRequest.body.difficulty,
          }
        }));
      });

      test('prep_time isNaN or < 0', async () => {
        mockRequest.body.prep_time = 'NaN';
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.prep_time = -1;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_PREP_TIME,
          details: {
            prep_time: mockRequest.body.prep_time,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('cook_time isNaN or < 0', async () => {
        mockRequest.body.cook_time = 'NaN';
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.cook_time = -1;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_COOK_TIME,
          details: {
            cook_time: mockRequest.body.cook_time,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('persons and isNan', async () => {
        mockRequest.body.persons = 'NaN';
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.persons = -1;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_PERSONS,
          details: {
            persons: mockRequest.body.persons,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('rest_time and (isNan or < 0)', async () => {
        mockRequest.body.rest_time = 'NaN';
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.rest_time = -1;
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_REST_TIME,
          details: {
            rest_time: mockRequest.body.rest_time,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('note and > 255', async () => {
        mockRequest.body.note = [...Array(256)].map(() => 'a').join('');
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_NOTE_TOO_LONG,
          details: {
            note: mockRequest.body.note,
          }
        }));

      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('createOne error');
        Recipe['create'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });

  });

  describe('patchOne', () => {
    const inBaseRecipeAttribute = {
      id: 1,
      name: 'My recipe',
      difficulty: 1,
      desc: 'My description',
      cook_time: 15,
      note: 'My note',
      persons: 2,
      prep_time: 25,
      user_id: 12,
    } as RecipeAttributes;

    const recipeUpdateBody = {
      name: 'My new name',
      desc: 'My new description',
      difficulty: 2,
      persons: 4,
      prep_time: 30,
      cook_time: 20,
      rest_time: 10,
      note: 'My new note',
    } as RecipeUpdateBody;

    beforeEach(() => {
      mockRequest.body = { ...recipeUpdateBody };
      mockRequest.user = {
        id: 12,
        role: 99,
        username: 'username',
      };

      const inBaseRecipe = Recipe.build(inBaseRecipeAttribute);
      inBaseRecipe['save'] = jest.fn();
      inBaseRecipe['reload'] = jest.fn();
      Recipe['findByPk'] = jest.fn().mockResolvedValue(inBaseRecipe);
    });

    describe('OK - 200', () => {
      test('recipe updated', async () => {
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', '/recipes/1');
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          ...recipeUpdateBody,
          id: 1,
          user_id: 12,
        }));
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('name and > 64', async () => {
        const longName = [...Array(65)].map(() => 'a').join('');
        mockRequest.body.name = longName;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_NAME_TOO_LONG,
          details: {
            name: longName,
          }
        }));
      });

      test('desc and > 255', async () => {
        const longDesc = [...Array(256)].map(() => 'a').join('');
        mockRequest.body.desc = longDesc;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_DESC_TOO_LONG,
          details: {
            description: longDesc,
          }
        }));
      });

      test('difficulty and is not 1, 2, 3', async () => {
        mockRequest.body.difficulty = 'NaN';
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_DIFFICULTY,
          details: {
            difficulty: mockRequest.body.difficulty,
          }
        }));
      });

      test('prep_time and isNaN or < 0', async () => {
        mockRequest.body.prep_time = 'NaN';
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.prep_time = -1;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_PREP_TIME,
          details: {
            prep_time: mockRequest.body.prep_time,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('rest_time and isNaN or < 0', async () => {
        mockRequest.body.rest_time = 'NaN';
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.rest_time = -1;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_REST_TIME,
          details: {
            rest_time: mockRequest.body.rest_time,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('cook_time and isNaN or < 0', async () => {
        mockRequest.body.cook_time = 'NaN';
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.cook_time = -1;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_COOK_TIME,
          details: {
            cook_time: mockRequest.body.cook_time,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('persons and isNan', async () => {
        mockRequest.body.persons = 'NaN';
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        mockRequest.body.persons = -1;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_PERSONS,
          details: {
            persons: mockRequest.body.persons,
          }
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });

      test('note and > 255', async () => {
        mockRequest.body.note = [...Array(256)].map(() => 'a').join('');
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_NOTE_TOO_LONG,
          details: {
            note: mockRequest.body.note,
          }
        }));
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('update not owned recipe', async () => {
        (mockRequest.user as JWTPayload).id = 10;
        await recipeController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'recipe'
          }
        }));
      });
    });
  });

  describe('deletOne', () => {
    const inBaseRecipeAttribute = {
      id: 1,
      name: 'My recipe',
      difficulty: 1,
      desc: 'My description',
      cook_time: 15,
      note: 'My note',
      persons: 2,
      prep_time: 25,
      user_id: 12,
    } as RecipeAttributes;

    let inBaseRecipe: Recipe;

    beforeEach(() => {
      mockRequest.body = undefined;
      mockRequest.user = {
        id: 12,
        role: 99,
        username: 'username',
      };

      inBaseRecipe = Recipe.build(inBaseRecipeAttribute);
      inBaseRecipe['destroy'] = jest.fn();
      Recipe['findByPk'] = jest.fn().mockResolvedValue(inBaseRecipe);
    });

    describe('OK - 200', () => {
      test('recipe deleted', async () => {
        inBaseRecipe['destroy'] = jest.fn().mockResolvedValueOnce(1);
        await recipeController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(200);
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('deleteOne error');
        inBaseRecipe['destroy'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('delete not owned recipe', async () => {
        (mockRequest.user as JWTPayload).id = 10;
        await recipeController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_DELETE,
          details: {
            ressource: 'recipe'
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('deleteOne error');
        inBaseRecipe['destroy'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });
  });

  describe('getIngredients', () => {
    describe('OK - 200', () => {
      test('send ingredients', async () => {
        const ingredient1 = Ingredient.build({
          id: 1,
          name: 'Farine'
        });
        const ingredient2 = Ingredient.build({
          id: 2,
          name: 'Oeufs'
        });
        const unit1 = QuantityUnit.build({
          id: 1,
          name: 'gramme',
          abbreviation: 'g',
        });
        const unit2 = QuantityUnit.build({
          id: 2,
          name: 'unit',
          abbreviation: '',
        });
        const expectedResult = [{
          quantity: 100,
          ingredient: ingredient1,
          unit: unit1,
        }, {
          quantity: 2,
          ingredient: ingredient2,
          unit: unit2,
        }] as Partial<Quantity>[];
        Quantity['findAll'] = jest.fn().mockResolvedValueOnce(expectedResult);
        await recipeController.getIngredients(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining(expectedResult));
      });
    });
    
    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('getIngredient error');
        Quantity['findAll'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.getIngredients(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
        expect(nextFunction).toBeCalledTimes(1);
      });
    });    
  });

  describe('putIngredient', () => {
    beforeEach(() => {
      mockRequest.user = {
        id: 12,
        role: 99,
        username: 'username',
      };
      mockRequest.body = {};

      Recipe['findByPk'] = jest.fn().mockResolvedValue({
        id: 1,
        user_id: 12,
      } as Recipe);
      Quantity['findOne'] = jest.fn();
    });
    
    describe('OK - 200', () => {
      test('created or updated ingredient quantity', async () => {
        mockRequest.body = {
          quantity: 500,
          unit_id: 1
        };
        const expectedQuantity = Quantity.build({
          ingredient_id: 2,
          recipe_id: 1,
          quantity: 500,
          unit_id: 1,
        });
        expectedQuantity['reload'] = jest.fn();
  
        Quantity['upsert'] = jest.fn().mockResolvedValueOnce([expectedQuantity]);
  
        await recipeController.putIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          ingredient_id: 2,
          recipe_id: 1,
          quantity: 500,
          unit_id: 1,
        }));
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('quantity isNaN or < 0', async () => {
        mockRequest.body = {
          quantity: 'trucdskqj',
        };
        await recipeController.putIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_QUANTITY,
          details: {
            quantity: mockRequest.body.quantity,
          }
        }));
      });

      test('unit_id isNaN or < 0', async () => {
        mockRequest.body = {
          quantity: 150,
          unit_id: -5,
        };
        await recipeController.putIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_WRONG_UNIT_ID,
          details: {
            unit_id: mockRequest.body.unit_id,
          }
        }));
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('put ingredient to not owned recipe', async () => {
        mockRequest.user = {
          id: 13,
          role: 99,
          username: 'username',
        };
        await recipeController.putIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'recipe',
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('putIngredient error');
        Recipe['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.putIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
    
  });

  describe('getIngredient', () => {
    describe('OK - 200', () => {
      test('send ingredient', async () => {
        const ingredient = Ingredient.build({
          id: 2,
          name: 'Farine',
        });
        const unit = QuantityUnit.build({
          id: 3,
          name: 'gramme',
          abbreviation: 'g',
        });
        const expectedResult = {
          quantity: 250,
          ingredient,
          unit,
        } as Partial<Quantity>;
        Quantity['findOne'] = jest.fn().mockResolvedValueOnce(expectedResult);
  
        await recipeController.getIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          quantity: 250,
          ingredient,
          unit,
        } as Quantity));
      });
    });

    describe('NOT_FOUND - 404', () => {
      test('ingredient not present in recipe', async () => {
        Quantity['findOne'] = jest.fn().mockResolvedValueOnce(null);
        await recipeController.getIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_INGREDIENT_NOT_IN,
          details: {
            ingredient_id: parseInt(mockRequest.params?.ingredientId as string),
            recipe_id: parseInt(mockRequest.params?.recipeId as string),
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('getIngredient error');
        Quantity['findOne'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.getIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });    
  });

  describe('removeIngredient', () => {
    const inBaseQuantity = Quantity.build({
      ingredient_id: 2,
      recipe_id: 1,
      quantity: 300,
      unit_id: 12,
    });
    inBaseQuantity['destroy'] = jest.fn();

    beforeEach(() => {
      mockRequest.user = {
        id: 12,
        role: 99,
        username: 'username',
      };

      Recipe['findByPk'] = jest.fn().mockResolvedValue({
        id: 1,
        user_id: 12,
      } as Recipe);
      Quantity['findOne'] = jest.fn().mockResolvedValue(inBaseQuantity);

    });

    describe('OK - 200', () => {
      test('remove ingredient', async () => {
        await recipeController.removeIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(200);
      });
    });
    

    describe('FORBIDDEN - 403', () => {
      test('not owned recipe', async () => {
        mockRequest.user = {
          id: 13,
          role: 99,
          username: 'username',
        };
        await recipeController.removeIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'recipe',
          }
        }));
      });
    });

    describe('NOT_FOUND - 404', () => {
      test('ingredient not present in recipe', async () => {
        Quantity['findOne'] = jest.fn().mockResolvedValueOnce(null);
        await recipeController.removeIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.RECIPE_INGREDIENT_NOT_IN,
          details: {
            ingredient_id: parseInt(mockRequest.params?.ingredientId as string),
            recipe_id: parseInt(mockRequest.params?.recipeId as string),
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should call next on error', async () => {
        const error = new Error('removeIngredient error');
        Recipe['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.removeIngredient(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });    
  });

  describe('getSteps', () => {
    const inBaseRecipeAttributes = {
      id: 1,
    } as RecipeAttributes;
    let inBaseRecipe: Recipe;
    const inBaseStepAssociation = [
      Step.build({
        id: 1,
        recipe_id: 1,
        order: 1,
        instruction: 'instruction 1',
      } as StepAttributes),
      Step.build({
        id: 4,
        recipe_id: 1,
        order: 2,
        instruction: 'instruction 2',
      } as StepAttributes)
    ];

    beforeEach(() => {
      inBaseRecipe = Recipe.build(inBaseRecipeAttributes);
      inBaseRecipe['getSteps'] = jest.fn().mockResolvedValueOnce(inBaseStepAssociation);
      Recipe['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseRecipe);
    });

    describe('OK - 200', () => {
      test('recipe steps', async () => {
        await recipeController.getSteps(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(inBaseStepAssociation);
      });
    });

    describe('NO_CONTENT - 204', () => {
      test('no steps', async () => {
        inBaseRecipe['getSteps'] = jest.fn().mockResolvedValueOnce([]);
        await recipeController.getSteps(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.sendStatus).toBeCalledWith(204);
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('getSteps error');
        Recipe['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await recipeController.getSteps(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });
});