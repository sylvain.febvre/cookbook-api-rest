import { IngredientBody, IngredientUpdateBody, QueryAcceptedFields } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { Ingredient } from '@database/models';
import { RequestHandler } from 'express';
import { CollectionController } from '../collection-controller';

class IngredientController extends CollectionController<Ingredient> {
  readonly acceptedFields: QueryAcceptedFields = {
    show: ['id', 'name'],
    sort: ['id', 'name'],
  };

  readonly createOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as IngredientBody;

      const name = body.name.trim();
      if (name.length > 20)
        return next(getApiError(ApiErrorCodes.INGREDIENT_NAME_TOO_LONG, { name }));

      const newIngredient = await this.model.create({
        name,
      });
      await newIngredient.reload();

      res.setHeader('Location', `/ingredients/${newIngredient.id}`);
      return res.status(201).json(newIngredient);
    } catch (e) {
      return next(e);
    }
  }

  readonly patchOne: RequestHandler = async (req, res, next) => {
    try {
      const body = req.body as IngredientUpdateBody;
      let { name } = body;

      const ingredient = await this.model.findByPk(req.params[this.paramName]) as Ingredient;

      if (name) {
        name = name.trim();
        if (name.length > 20)
          return next(getApiError(ApiErrorCodes.INGREDIENT_NAME_TOO_LONG, { name }));
        ingredient.name = name;
      }

      await ingredient.save();
      await ingredient.reload();

      res.setHeader('Location', `/ingredients/${ingredient.id}`);
      return res.json(ingredient);
    } catch (e) {
      return next(e);
    }
  }

}

export const ingredientController = new IngredientController({
  paramName: 'ingredientId'
});