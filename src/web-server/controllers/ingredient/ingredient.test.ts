import { Ingredient } from '@database/models';
import { Request, Response } from 'express';
import { ingredientController } from '@api/controllers';
import { ApiError, ApiErrorCodes } from '@api/utils/error';

describe('ingredientController handlers', () => {
  const mockRequest: Partial<Request> = {
    body: {},
    params: {
      ingredientId: '1'
    },
    api: {
      findOptions: {}
    }
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
    mockRequest.body = {};
    mockResponse.json = jest.fn();
    mockResponse.sendStatus = jest.fn();
    mockResponse.status = jest.fn().mockReturnThis();
    mockResponse.setHeader = jest.fn();
  });

  describe('createOne', () => {

    beforeEach(() => {
      mockRequest.body = {
        id: 1,
        name: 'Farine ',
      };
    });

    describe('CREATED - 201', () => {
      test('create ingredient', async () => {
        const expectedCreateAttributes = {
          id: 1,
          name: 'un',
          desc: 'one two three',
        };
        const newExpectedIngredient = Ingredient.build(expectedCreateAttributes);
        newExpectedIngredient['reload'] = jest.fn().mockResolvedValueOnce(undefined);
        Ingredient['create'] = jest.fn().mockResolvedValueOnce(newExpectedIngredient);
        await ingredientController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/ingredients/${newExpectedIngredient.id}`);
        expect(mockResponse.status).toBeCalledWith(201);
        expect(mockResponse.json).toBeCalledWith(newExpectedIngredient);
      });
    });
    

    describe('BAD_REQUEST - 400', () => {
      test('name.length > 20', async () => {
        mockRequest.body = {
          id: 1,
          name: 'nameistoolonghshjdthe',
        };
        await ingredientController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.INGREDIENT_NAME_TOO_LONG,
          details: {
            name: mockRequest.body.name,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should send Error on error', async () => {
        mockRequest.body = {
          id: 1,
          name: 'john',
        };
        const error = new Error('createOne error');
        Ingredient['create'] = jest.fn().mockRejectedValueOnce(error);
        await ingredientController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('patchOne', () => {
    const inBaseIngredientAttribute = {
      id: 1,
      name: 'old name',
    };

    beforeEach(() => {
      const inBaseIngredient = Ingredient.build(inBaseIngredientAttribute);
      inBaseIngredient['save'] = jest.fn();
      inBaseIngredient['reload'] = jest.fn();
      Ingredient['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseIngredient);
    });

    describe('OK - 200', () => {
      test('patch ingredient', async () => {
        mockRequest.body = {
          name: 'new name    ',
        };
  
        await ingredientController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/ingredients/1`);
        expect(mockResponse.json).toBeCalledWith(expect.objectContaining({
          id: 1,
          name: 'new name',
        }));
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('name.length > 20', async () => {
        mockRequest.body = {
          name: 'new nameistoolonghshjdthe',
        };
        await ingredientController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.INGREDIENT_NAME_TOO_LONG,
          details: {
            name: mockRequest.body.name,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('generic error', async () => {
        const error = new Error('patchOne error');
        Ingredient['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await ingredientController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });
});