import { QueryAcceptedFields } from '@api/utils';
import { sequelize } from '@database';
import { Router } from 'express';
import { RequestHandler } from 'express-serve-static-core';
import { Model, ModelStatic } from 'sequelize';
import { checkIfExists } from '@api/middlewares';

export interface ICollectionControllerOptions {
  /**
   * Name of the url id parameter
   */
  paramName?: string;

  /**
   * Name of the database model
   */
  modelName?: string;

  /**
   * Fields accepted in query options
   */
  // acceptedFields?: QueryAcceptedFields;
}

export interface ICollectionController<M extends Model> {


  /**
   * Router representing all routes for the collection
   * /collection
   */
  collectionRouter: Router;
  
  /**
   * Router representing all routes for one ressource
   * Automatically append id on the route and check if ressource exists
   * /collection/:id
   */
  ressourceRouter: Router;

  /**
   * Name of the url id parameter
   */
  paramName: string;

  /**
   * Name of the database model
   */
  model: ModelStatic<M>;

  /**
   * Fields accepted in query options
   */
  acceptedFields: QueryAcceptedFields;
}

export interface ICollectionBaseCallbacks {
  /**
   * Handler to get a list of ressources
   * 
   */
  getAll: RequestHandler;

  /**
   * Handler to get on ressource
   */
  getOne: RequestHandler;

  /**
   * Handler to create a new ressource
   * No default implementation
   */
  createOne: RequestHandler;

  /**
   * Handler to delete one ressource
   */
  deleteOne: RequestHandler;

  /**
   * Handler to patch one ressource
   * No default implementation
   */
  patchOne: RequestHandler;
}

/**
 * Abstract class to manage the base route handlers
 */
export abstract class CollectionController<M extends Model> implements ICollectionController<M>, ICollectionBaseCallbacks {

  /**
   * The collection router
   */
  #collection = Router();
  get collectionRouter(): Router {
    return this.#collection;
  }

  /**
   * The ressource router
   */
  #ressource = Router({ mergeParams: true });
  get ressourceRouter(): Router {
    return this.#ressource;
  }

  /**
   * Name of the url parameter in express route ( :{paramName} )
   */
  readonly paramName: string;

  /**
   * The sequelize model matching the ressource
   */
  readonly model: ModelStatic<M>;

  /**
   * Fields accepted in the url query parameters (in get method)
   */
  abstract readonly acceptedFields: QueryAcceptedFields;

  constructor(options: ICollectionControllerOptions = {}) {
    this.paramName = options.paramName || `${this.constructor.name}Id`;
    let modelName;
    if (options.modelName) {
      modelName = options.modelName;
      this.model = sequelize.models[modelName] as ModelStatic<M>;
      if (!this.model)
        throw new Error(`Model ${modelName} doesn't exist in sequelize`);
    } else {
      // try to infer modelName from class name if not provided
      const modelNameRegexp = /[A-Z][a-z]+/;
      const modelNameMatch = this.constructor.name.match(modelNameRegexp);
      if (!modelNameMatch)
        throw new Error(`Can't infer model name from class name ${this.constructor.name}`);
      modelName = modelNameMatch[0];
      this.model = sequelize.models[modelName] as ModelStatic<M>;
      if (!this.model)
        throw new Error(`Model ${modelName} infered from class name doesn't exist in sequelize`);
    }

    this.#collection.param(this.paramName, checkIfExists(this.model));
    this.#collection.use(`/:${this.paramName}(\\d+)`, this.#ressource);
  }

  /**
   * Base implementation of the handler for the /models/ route (GET)
   */
  readonly getAll: RequestHandler = async (req, res, next) => {
    try {
      const models = await this.model.findAll(req.api.findOptions);

      const totalCount = await this.model.count();

      if (models.length <= 0) {
        return res.sendStatus(204);
      }

      res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
      res.setHeader('X-Total-Count', totalCount);
      return res.json(models);

    } catch (e) {
      return next(e);
    }
  }

  /**
   * Implementation of the handler for the /models/ route (POST)
   */
  abstract readonly createOne: RequestHandler;

  /**
   * Base implementation of the handler for the /models/:{paramName} route (GET)
   */
  readonly getOne: RequestHandler = async (req, res, next) => {
    try {
      const model = await this.model.findByPk(req.params[this.paramName], req.api.findOptions);

      return res.json(model);
    } catch (e) {
      return next(e);
    }
  }

  /**
   * Base implementation of the handler for the /models/:{paramName} route (DELETE)
   */
  readonly deleteOne: RequestHandler = async (req, res, next) => {
    try {
      await this.model.destroy({
        where: { id: req.params[this.paramName] },
        limit: 1,
      });
      return res.sendStatus(200);
    } catch (e) {
      return next(e);
    }
  };

  /**
   * Implementation of the handler for the /models/:{paramName} route (PUT)
   */
  abstract readonly patchOne: RequestHandler;
}