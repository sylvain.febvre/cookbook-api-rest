import { ROLES } from '@api/middlewares';
import { JWTPayload, QueryAcceptedFields, StepBody, StepUpdateBody } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { sequelize } from '@database';
import { Recipe, Step } from '@database/models';
import { RequestHandler } from 'express';
import { Op } from 'sequelize';
import { CollectionController } from '../collection-controller';


const queryFields = ['id', 'recipe_id', 'order', 'instruction'];

class StepController extends CollectionController<Step> {
  readonly acceptedFields: QueryAcceptedFields = {
    show: queryFields,
    sort: queryFields,
  }

  readonly createOne: RequestHandler = async (req, res, next) => {
    const transaction = await sequelize.transaction();
    try {
      const body = req.body as StepBody;
      const payload = req.user as JWTPayload;

      // Check attributes

      // recipe
      const recipe_id = parseInt(body.recipe_id as unknown as string, 10);
      if (isNaN(recipe_id))
        return next(getApiError(ApiErrorCodes.STEP_WRONG_RECIPE_ID, { recipe_id: body.recipe_id }));

      // Recipe exists
      const recipe = await Recipe.findByPk(recipe_id, {
        attributes: ['id', 'user_id']
      });
      if (!recipe)
        return next(getApiError(ApiErrorCodes.STEP_RECIPE_NOT_FOUND, { recipe_id }));

      // Check user own recipe or is admin
      if (recipe.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
        return next(getApiError(ApiErrorCodes.STEP_FORBIDDEN_CREATE, { recipe_id }));
      }

      // order
      let order = body.order ? parseInt(body.order as unknown as string, 10) : undefined;
      if (order && (isNaN(order) || order < 1))
        return next(getApiError(ApiErrorCodes.STEP_WRONG_ORDER, { order }));

      // instruction
      const instruction = body.instruction.trim();
      if (instruction.length > 512)
        return next(getApiError(ApiErrorCodes.STEP_INSTRUCTION_TOO_LONG, { instruction }));

      const stepCount = await Step.count({
        where: {
          recipe_id,
        }
      });

      if (stepCount <= 0) {
        // There is no other step, first step
        order = 1;
      } else {
        if (!order || order > stepCount) {
          // This is the last step
          order = stepCount + 1;
        } else {
          // insert before last, each step after this one => order + 1
          await Step.increment({ order: 1 }, {
            where: {
              recipe_id,
              order: {
                [Op.gte]: order,
              }
            },
            transaction,
          });
        }
      }

      const newStep = await this.model.create({
        recipe_id,
        instruction,
        order,
      }, {
        transaction,
      });
      await newStep.reload({
        transaction
      });
      await transaction.commit();

      res.setHeader('Location', `/steps/${newStep.id}`);
      return res.status(201).json(newStep);
    } catch (e) {
      await transaction.rollback();
      return next(e);
    }
  }

  readonly patchOne: RequestHandler = async (req, res, next) => {
    const transaction = await sequelize.transaction();
    try {
      // can only change instruction here (not order)
      const payload = req.user as JWTPayload;
      const body = req.body as StepUpdateBody;

      // get the step with user_id from recipe
      const step = await this.model.findByPk(req.params[this.paramName], {
        include: {
          association: 'recipe',
          attributes: ['user_id'],
        }
      }) as Step;

      // Check user own recipe or is admin
      if (step.recipe?.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
        return next(getApiError(ApiErrorCodes.FORBIDDEN_UPDATE, { ressource: 'step' }));
      }

      // Check instruction
      const instruction = body.instruction?.trim();
      if (instruction) {
        if (instruction.length > 512)
          return next(getApiError(ApiErrorCodes.STEP_INSTRUCTION_TOO_LONG, { instruction }));
        step.instruction = instruction;
      }

      // order
      let order = body.order;
      if (order) {
        order = parseInt(order as unknown as string, 10);

        if (isNaN(order) || order < 1)
          return next(getApiError(ApiErrorCodes.STEP_WRONG_ORDER, { order }));

        const stepCount = await Step.count({
          where: {
            recipe_id: step.recipe_id,
          }
        });

        if (order > stepCount) {
          order = stepCount;
        }
        if (stepCount > 1 && order !== step.order) {
          // More than one step and order changed, we check order
          // We need to check if order is up or down
          if (order > step.order) {
            // we move up
            await Step.increment({ order: -1 }, {
              where: {
                recipe_id: step.recipe_id,
                order: {
                  [Op.between]: [step.order + 1, order],
                },
              },
              transaction,
            });
          } else {
            // we move down
            await Step.increment({ order: 1 }, {
              where: {
                recipe_id: step.recipe_id,
                order: {
                  [Op.between]: [order, step.order - 1],
                },
              },
              transaction,
            });
          }
          // Set the new order to the step
          step.order = order;
        }
      }


      await step.save();
      await step.reload({
        include: [],
        transaction,
      });
      await transaction.commit();

      res.setHeader('Location', `/steps/${step.id}`);
      return res.json(step);
    } catch (e) {
      await transaction.rollback();
      return next(e);
    }
  }

  readonly deleteOne: RequestHandler = async (req, res, next) => {
    const transaction = await sequelize.transaction();
    try {
      // We need to sync order when we delete a step !
      const payload = req.user as JWTPayload;

      // get the step with user_id from recipe
      const step = await this.model.findByPk(req.params[this.paramName], {
        include: {
          association: 'recipe',
          attributes: ['user_id'],
        }
      }) as Step;

      // Check user own recipe or is admin
      if (step.recipe?.user_id !== payload.id && payload.role !== ROLES.ADMIN) {
        return next(getApiError(ApiErrorCodes.FORBIDDEN_DELETE, { ressource: 'step' }));
      }

      const stepCount = await Step.count({
        where: {
          recipe_id: step.recipe_id,
        }
      });

      // We delete before last step
      if (step.order < stepCount) {
        // Decrement from the next step to the last one
        await Step.increment({ order: -1 }, {
          where: {
            recipe_id: step.recipe_id,
            order: {
              [Op.gt]: step.order,
            },
          },
          transaction,
        });
      }

      await this.model.destroy({
        where: { id: req.params[this.paramName] },
        limit: 1,
        transaction,
      });

      await transaction.commit();
      return res.sendStatus(200);
    } catch (e) {
      await transaction.rollback();
      return next(e);
    }
  }
}

export const stepController = new StepController({
  paramName: 'stepId',
});