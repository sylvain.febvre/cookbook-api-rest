import { StepBody } from '@api/utils';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { sequelize } from '@database';
import { Recipe } from '@database/models';
import { Step, StepAttributes } from 'database/models/step.model';
import { Request, Response } from 'express';
import { stepController } from './step';

describe('stepController handlers', () => {

  const mockRequest: Partial<Request> = {
    body: {},
    params: {
      stepId: '1'
    },
    api: {
      findOptions: {}
    }
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
    mockRequest.body = {};
    mockResponse.json = jest.fn();
    mockResponse.sendStatus = jest.fn();
    mockResponse.status = jest.fn().mockReturnThis();
    mockResponse.setHeader = jest.fn();
    mockRequest.user = {
      id: 1,
      role: 99,
      username: 'toto'
    };
  });

  describe('createOne', () => {

    beforeEach(() => {
      mockRequest.body = {
        recipe_id: 1,
        order: 1,
        instruction: 'Une instruction  ',
      } as StepBody;
      Recipe['findByPk'] = jest.fn().mockResolvedValue({
        id: 1,
        user_id: 1,
      } as Recipe);
      sequelize['transaction'] = jest.fn().mockImplementation(() => {
        return {
          commit: jest.fn(),
          rollback: jest.fn(),
        };
      });
    });

    describe('CREATED - 201', () => {
      const expectedCreateAttributes = {
        id: 1,
        recipe_id: 1,
        order: 1,
        instruction: 'Une instruction',
      } as StepAttributes;
      const newExpectedStep = Step.build(expectedCreateAttributes);
      newExpectedStep['reload'] = jest.fn();
      Step['create'] = jest.fn().mockResolvedValue(newExpectedStep);

      test('1st step', async () => {
        mockRequest.body.order = 1;
        Step['count'] = jest.fn().mockResolvedValueOnce(0);
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(newExpectedStep);
      });

      test('last step', async () => {
        mockRequest.body.order = 3;
        Step['count'] = jest.fn().mockResolvedValueOnce(1);
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(newExpectedStep);
      });

      test('insert before last', async () => {
        mockRequest.body.order = 2;
        Step['count'] = jest.fn().mockResolvedValueOnce(3);
        Step['increment'] = jest.fn();
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(newExpectedStep);
        expect(Step.increment).toBeCalled();
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('recipe_id is NaN', async () => {
        mockRequest.body.recipe_id = 'azerty';
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_WRONG_RECIPE_ID,
          details: {
            recipe_id: mockRequest.body.recipe_id,
          }
        }));
      });

      test('recipe not present', async () => {
        Recipe['findByPk'] = jest.fn();
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_RECIPE_NOT_FOUND,
          details: {
            recipe_id: mockRequest.body.recipe_id,
          }
        }));
      });

      test('order is NaN || < 1', async () => {
        mockRequest.body.order = -1;
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_WRONG_ORDER,
          details: {
            order: mockRequest.body.order,
          }
        }));
      });

      test('instruction length > 512', async () => {
        const longInstruction = [...Array(513)].map(() => 'a').join('');
        mockRequest.body.instruction = longInstruction;
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_INSTRUCTION_TOO_LONG,
          details: {
            instruction: mockRequest.body.instruction,
          }
        }));
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('user does not own recipe', async () => {
        mockRequest.user = {
          id: 12,
          role: 99,
          username: 'tata'
        };
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_FORBIDDEN_CREATE,
          details: {
            recipe_id: mockRequest.body.recipe_id,
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should send Error on error', async () => {
        const error = new Error('createOne error');
        Recipe['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await stepController.createOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('patchOne', () => {
    const inBaseStepAttribute = {
      id: 1,
      recipe_id: 1,
      order: 3,
      instruction: 'Une instruction',
      recipe: {
        user_id: 1
      }
    } as StepAttributes;
    let inBaseStep: Step;

    beforeEach(() => {
      inBaseStep = inBaseStepAttribute as Step;
      inBaseStep['save'] = jest.fn();
      inBaseStep['reload'] = jest.fn();
      Step['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseStep);
      Step['increment'] = jest.fn();
    });
    
    describe('OK - 200', () => {
      beforeEach(() => {
        Step['count'] = jest.fn().mockResolvedValueOnce(5);
      });

      test('instruction is updated', async () => {
        mockRequest.body = {
          instruction: 'new instruction    ',
        };
  
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.setHeader).toBeCalledWith('Location', `/steps/1`);
        expect(mockResponse.json).toBeCalledWith(inBaseStep);
        expect(inBaseStep.instruction).toEqual('new instruction');
      });

      test('new order is > step order', async () => {
        mockRequest.body.order = 6;
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(inBaseStep);
        expect(inBaseStep.order).toEqual(5);
      });

      test('new order is < step order', async () => {
        mockRequest.body.order = 1;
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(mockResponse.json).toBeCalledWith(inBaseStep);
        expect(inBaseStep.order).toEqual(1);
      });
    });

    describe('BAD_REQUEST - 400', () => {
      test('instruction length > 512', async () => {
        const longInstruction = [...Array(513)].map(() => 'a').join('');
        mockRequest.body.instruction = longInstruction;
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_INSTRUCTION_TOO_LONG,
          details: {
            instruction: mockRequest.body.instruction,
          }
        }));
      });

      test('order < 1 or NaN', async () => {
        mockRequest.body.order = -5;
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.STEP_WRONG_ORDER,
          details: {
            order: mockRequest.body.order,
          }
        }));
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('user does not own recipe', async () => {
        mockRequest.user = {
          id: 12,
          role: 99,
          username: 'tata'
        };
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_UPDATE,
          details: {
            ressource: 'step',
          }
        }));
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should send Error on error', async () => {
        const error = new Error('patchOne error');
        Step['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await stepController.patchOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('deleteOne', () => {
    const inBaseStepAttribute = {
      id: 1,
      recipe_id: 1,
      order: 3,
      instruction: 'Une instruction',
      recipe: {
        user_id: 1
      }
    } as StepAttributes;
    let inBaseStep: Step;

    beforeEach(() => {
      inBaseStep = inBaseStepAttribute as Step;
      inBaseStep['save'] = jest.fn();
      inBaseStep['reload'] = jest.fn();
      Step['findByPk'] = jest.fn().mockResolvedValueOnce(inBaseStep);
      Step['increment'] = jest.fn();
      Step['count'] = jest.fn();
      Step['destroy'] = jest.fn();
   });

    describe('OK - 200', () => {
      test('delete step and sync order', async () => {
        Step['count'] = jest.fn().mockResolvedValueOnce(6);
        await stepController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(Step.increment).toBeCalled();
        expect(Step.destroy).toBeCalled();
        expect(mockResponse.sendStatus).toBeCalledWith(200);
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('user does not own recipe', async () => {
        mockRequest.user = {
          id: 12,
          role: 99,
          username: 'tata',
        };
        await stepController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN_DELETE,
          details: {
            ressource: 'step',
          }
        }));  
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      it('should send Error on error', async () => {
        const error = new Error('move error');
        Step['findByPk'] = jest.fn().mockRejectedValueOnce(error);
        await stepController.deleteOne(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });
});