import { Request, Response, NextFunction } from 'express';
import { pingController, WebServer } from './web-server';
import { sequelize } from '@database';

jest.mock('@logger');

describe('WebServer', () => {
  let webServer: WebServer;

  beforeEach(() => {
    webServer = new WebServer();
  });

  it('should launch then close webserver', async () => {
    const start = await webServer.initialize();
    expect(start).toEqual(0);
    const close = await webServer.close();
    expect(close).toEqual(0);
  });

  it('should throw "ERR_SERVER_ALREADY_LISTEN" if server is already started', async () => {
    await webServer.initialize();
    try {
      await webServer.initialize();
    } catch (err) {
      expect((err as NodeJS.ErrnoException).code).toEqual('ERR_SERVER_ALREADY_LISTEN');
    }
    await webServer.close();
  });

  it('should throw "ERR_SERVER_NOT_RUNNING" if server is not started', async () => {
    try {
      await webServer.close();
    } catch (err) {
      expect((err as NodeJS.ErrnoException).code).toEqual('ERR_SERVER_NOT_RUNNING');
    }
  });

  describe('pingController', () => {
    const mockRequest: Partial<Request> = {};
    const mockResponse: Partial<Response> = {
      json: jest.fn(),
    };
    const nextFunction: NextFunction = jest.fn();

    beforeEach(() => {
      jest.resetAllMocks();
    });

    it('should return expected values', async () => {
      jest.spyOn(sequelize, 'authenticate').mockImplementation(() => {
        return Promise.resolve();
      });

      await pingController(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(mockResponse.json).toBeCalledWith({
        webServer: 'Ok !',
        sequelize: 'Ok !',
      });
    });

    it('should call next function with error', async () => {
      const error = new Error('Query Error');
      jest.spyOn(sequelize, 'authenticate').mockImplementation(() => {
        throw error;
      });

      await pingController(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith<Error[]>(error);

    });
  });
});

/* describe('Webserver request flow', () => {
  let webServer: WebServer;
  beforeAll(async () => {
    jest.restoreAllMocks();
    webServer = new WebServer();
    await webServer.initialize();
  });

  afterAll(async () => {
    await webServer.close();
  });

  it(`should return 200, test response and log request`, done => {
    jest.spyOn(sequelize, 'authenticate').mockImplementation(() => {
      return Promise.resolve();
    });
    request(webServer.express)
      .get('/test')
      .expect('Content-Type', /application\/json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body).toEqual({
          webServer: 'Ok !',
          sequelize: 'Ok !',
        });
        expect(logReq.info).toHaveBeenCalled();
        done();
      });
  });
}); */