import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { RequestHandler } from 'express';

export const notImplemented: RequestHandler = (req, res, next) => {
  return next(getApiError(ApiErrorCodes.NOT_IMPLEMENTED));
};