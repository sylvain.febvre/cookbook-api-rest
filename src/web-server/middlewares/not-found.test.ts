import { Request, Response, NextFunction } from 'express';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { notFound } from '.';

describe('notFound middleware', () => {
  it('should send NOT_FOUND error', () => {
    const mockRequest: Partial<Request> = {
      originalUrl: '/toto/tata',
      protocol: 'https',
      hostname: 'hostname'
    };
    const mockResponse: Partial<Response> = {};
    const nextFunction: NextFunction = jest.fn();
    notFound(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
      code: ApiErrorCodes.NOT_FOUND,
      details: {
        route: '/toto/tata'
      }
    }));
  });
});