import { Request, Response } from 'express';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { requiredBodyParams } from './required-body-params';

describe('requiredBodyParams middleware', () => {
  const mockRequest: Partial<Request> = {
    method: 'POST',
    body: {
      p1: 1,
      p2: 'two',
      p9: 9,
    },
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
  });

  it('should call next without error', () => {
    requiredBodyParams('p1', 'p2')(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith();
    expect(nextFunction).toBeCalledTimes(1);
  });

  it('should send BAD_REQUEST error if a required param is missing in body', () => {
    const requiredParams = ['p1', 'p2', 'p3'];
    requiredBodyParams(...requiredParams)(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
      code: ApiErrorCodes.REQUIRED_BODY_PARAMS,
      details: { params: requiredParams}
    }));
    expect(nextFunction).toBeCalledTimes(1);
  });
});