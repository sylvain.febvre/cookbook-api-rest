import { logApp } from '@logger';
import { ApiError, ApiErrorCodes, getApiError } from '@api/utils/error';
import { ErrorRequestHandler } from 'express';
import { BaseError } from 'sequelize';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorHandler: ErrorRequestHandler = (err: Error, req, res, next) => {
  let apiError: ApiError;
  if(err instanceof ApiError) {
    apiError = err;
  } else {
    logApp.error(err.stack);
    apiError = getApiError((err instanceof BaseError) ? ApiErrorCodes.DATABASE : ApiErrorCodes.SERVER, {error: err.message});
  }

  // We get the keys we need (message is part of Error and can be get by destruct)
  const errorAsJson = {...apiError, message: apiError.message};
  return res.status(errorAsJson.status).json(errorAsJson);
};
