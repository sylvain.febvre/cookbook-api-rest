import { JWTPayload } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { JWT, User } from '@database/models';
import { createHmac } from 'crypto';
import { RequestHandler } from 'express';
import jwt, { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';

const env = process.env;

export const checkAuth: RequestHandler = async (req, res, next) => {
  const authHeader = req.get('authorization');
  // Header exists
  if (!authHeader) {
    return next(getApiError(ApiErrorCodes.AUTH_NO_HEADER));
  }

  const parts = authHeader.split(' ');
  // Correct format ("Bearer {token}")
  if (parts.length !== 2 || parts[0] !== 'Bearer') {
    return next(getApiError(ApiErrorCodes.AUTH_WRONG_HEADER));
  }

  const token = parts[1];
  try {
    const payload = jwt.verify(token, env.JWT_SECRET) as JWTPayload;

    // Check if user exists in db
    const user = await User.findByPk(payload.id, {
      attributes: {
        include: ['role_id'],
      },
      include: 'JWTs',
    });

    if (!user) {
      return next(getApiError(ApiErrorCodes.AUTH_USER_NOT_FOUND));
    }

    // Check JWT is valid (in db), user could be ban or deactivate
    const hasJwt = user.JWTs?.find(jwt => jwt.hash === createHmac('sha512', 'token').update(token).digest('hex'));

    if (!hasJwt) {
      return next(getApiError(ApiErrorCodes.AUTH_TOKEN_INVALID));
    }

    req.user = {
      id: user.id,
      role: user.role_id,
      username: user.username,
    };
    return next();

  } catch (jwtErr) {
    // Failed to verify token
    const error = jwtErr as Error;
    if (error instanceof JsonWebTokenError) {
      if (error instanceof TokenExpiredError) {
        // Token is expired, remove it from db
        const payload = jwt.decode(token) as JWTPayload;
        await JWT.destroy({
          where: {
            user_id: payload.id,
            hash: createHmac('sha512', 'token').update(token).digest('hex'),
          }
        });
        return next(getApiError(ApiErrorCodes.AUTH_TOKEN_EXPIRED));
      } else {
        return next(getApiError(ApiErrorCodes.AUTH_INVALID_SIGN));
      }
    } else {
      return next(error);
    }

  }
};

export const checkRole = (...roles: ROLES[]): RequestHandler => {
  return (req, res, next) => {
    console.log(req.user);
    // Check if user is auth
    if (!req.user)
      return next(getApiError(ApiErrorCodes.NOT_SIGNED));

    // Check role
    if (req.user.role !== ROLES.ADMIN && !roles.includes(req.user.role))
      return next(getApiError(ApiErrorCodes.FORBIDDEN));

    return next();
  };
};

export const enum ROLES {
  ADMIN = 0,

  USER = 99,
}