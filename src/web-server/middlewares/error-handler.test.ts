import { Request, Response, NextFunction } from 'express';
import { errorHandler } from './error-handler';

import { ApiError, ApiErrorCodes, getApiError } from '@api/utils/error';
import { BaseError } from 'sequelize';

jest.mock('@logger');

describe('errorHandler middleware', () => {
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockError: ApiError | Error;
  const nextFunction: NextFunction = jest.fn();

  beforeEach(() => {
    mockRequest = {
      method: 'GET',
    };
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  it('should send 500 - Internal Server Error with Database Error message if error is an instance of BaseError', () => {
    mockError = new BaseError('Test Error');
    errorHandler(mockError, mockRequest as Request, mockResponse as Response, nextFunction);
    expect(mockResponse.status).toBeCalledWith(500);
    expect(mockResponse.json).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
      code: ApiErrorCodes.DATABASE,
      details: {
        error: mockError.message,
      }
    }));
  });

  it('should send error as it if instance of ApiError', () => {
    const mockError: ApiError = getApiError(ApiErrorCodes.NOT_FOUND, {
      testError: 'testError',
    });
    errorHandler(mockError, mockRequest as Request, mockResponse as Response, nextFunction);
    expect(mockResponse.status).toBeCalledWith(404);
    expect(mockResponse.json).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
      code: ApiErrorCodes.NOT_FOUND,
      details: {
        testError: 'testError',
      }
    }));
  });

  it('should send 500 - Internal Server Error if error is another error', () => {
    mockError = new Error('Test Error');
    errorHandler(mockError, mockRequest as Request, mockResponse as Response, nextFunction);
    expect(mockResponse.status).toBeCalledWith(500);
    expect(mockResponse.json).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
      code: ApiErrorCodes.SERVER,
      details: {
        error: mockError.message,
      }
    }));
  });
});