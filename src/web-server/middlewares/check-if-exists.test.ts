import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { Request, Response } from 'express';
import { Model, ModelStatic } from 'sequelize';
import { checkIfExists } from './check-if-exists';

describe('Check if exists param handler', () => {
  const mockRequest: Partial<Request> = {
    originalUrl: '/original/url',
    headers: {
      hostname: 'toto',
      protocol: 'http',
    },
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();
  const mockModel: Partial<ModelStatic<Model>> & { name: string } = {
    name: 'MockModel'
  };

  beforeEach(() => {
    nextFunction.mockRestore();
  });

  it('should call next without error', async () => {
    mockModel.findByPk = jest.fn().mockResolvedValue(true);
    await checkIfExists(mockModel as ModelStatic<Model>)(mockRequest as Request, mockResponse as Response, nextFunction, 2, 'modelId');
    expect(nextFunction).toBeCalledWith();
  });

  describe('BAD_REQUEST - 400', () => {
    test('param isNan or < 0', async () => {
      mockModel.findByPk = jest.fn().mockResolvedValue(true);
      await checkIfExists(mockModel as ModelStatic<Model>)(mockRequest as Request, mockResponse as Response, nextFunction, 'azerty', 'modelId');
      expect(nextFunction).toHaveBeenNthCalledWith(1, expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.CHECK_EXISTS_WRONG_ID,
        details: {
          modelId: 'azerty',
        },
      }));
      await checkIfExists(mockModel as ModelStatic<Model>)(mockRequest as Request, mockResponse as Response, nextFunction, -2, 'modelId');
      expect(nextFunction).toHaveBeenNthCalledWith(2, expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.CHECK_EXISTS_WRONG_ID,
        details: {
          modelId: -2
        },
      }));
      expect(nextFunction).toHaveBeenCalledTimes(2);
    });
  });

  describe('NOT_FOUND - 404', () => {
    test('ressource is not found in base', async () => {
      mockModel.findByPk = jest.fn().mockResolvedValue(null);
      await checkIfExists(mockModel as ModelStatic<Model>)(mockRequest as Request, mockResponse as Response, nextFunction, 5, 'modelId');
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.CHECK_EXISTS_NOT_FOUND,
        details: {
          ressource: mockModel.name,
          route: '/original/url'
        }
      }));
    });
  });
  
  describe('INTERNAL_SERVER_ERROR - 500', () => {
    test('generic', async () => {
      const error = new Error('Generic Error');
      mockModel.findByPk = jest.fn().mockRejectedValue(new Error('Generic Error'));
      await checkIfExists(mockModel as ModelStatic<Model>)(mockRequest as Request, mockResponse as Response, nextFunction, 22, 'otherId');
      expect(nextFunction).toBeCalledWith(error);
    });
  });
});