import { Request, Response } from 'express';
import { addQueryOptions } from './query-options';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { QueryAcceptedFields } from '@api/utils';
import { DefaultFindOptions } from '@database';

describe('query options middleware', () => {
  const mockRequest: Partial<Request> = {
    method: 'GET',
    query: {},
  };
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();
  const queryFields: QueryAcceptedFields = {
    show: ['id', 'field_1', 'field_2'],
    include: ['tests'],
    sort: ['id', 'field_1']
  };

  beforeEach(() => {
    mockRequest.query = {};
    mockRequest.api = {
      findOptions: { ...DefaultFindOptions }
    };
    nextFunction.mockRestore();
  });

  it('should set attributes option', () => {
    mockRequest.query = {
      fields: 'id,field_2',
    };
    addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith();
    expect(mockRequest.api?.findOptions.attributes).toIncludeSameMembers(['id', 'field_2']);
  });

  it('should remove duplicate and set include option', () => {
    mockRequest.query = {
      include: 'tests,tests'
    };
    addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith();
    expect(mockRequest.api?.findOptions.include).toIncludeSameMembers(['tests']);
  });

  it('should set order option', () => {
    mockRequest.query = {
      sort: 'id.asc,field_1.desc'
    };
    addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith();
    expect(mockRequest.api?.findOptions.order).toIncludeSameMembers([['id', 'asc'], ['field_1', 'desc']]);
  });

  it('should set limit option', () => {
    mockRequest.query = {
      limit: '30'
    };
    addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith();
    expect(mockRequest.api?.findOptions.limit).toEqual(30);
  });

  it('should set offset option', () => {
    mockRequest.query = {
      page: '3'
    };
    addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith();
    expect(mockRequest.api?.findOptions.offset).toEqual(40);
  });

  describe('BAD_REQUEST - 400', () => {
    test('wrong selected fields', () => {
      mockRequest.query = {
        fields: 'not,in,fields'
      };
      addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.QUERY_OPTIONS_FIELDS,
        details: { acceptedFields: queryFields.show as string[]}
      }));
    });

    test('wrong include', () => {
      mockRequest.query = {
        include: 'wrong'
      };
      addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.QUERY_OPTIONS_INCLUDE,
        details: { acceptedInclude: queryFields.include as string[]}
      }));
    });

    test('wrong sort fields', () => {
      mockRequest.query = {
        sort: 'id,wrong'
      };
      addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.QUERY_OPTIONS_SORT,
        details: { acceptedFields: queryFields.sort as string[]}
      }));
    });

    test('sort fields are not in selected fields', () => {
      mockRequest.query = {
        fields: 'id,field_2',
        sort: 'field_1'
      };
      addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.QUERY_OPTIONS_SORT_SELECTED,
        details: { acceptedFields: mockRequest.api?.findOptions.attributes as string[]}
      }));
    });

    test('limit field isNaN', () => {
      mockRequest.query = {
        limit: 'NaN'
      };
      addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.QUERY_OPTIONS_LIMIT,
      }));
    });

    test('page field isNaN', () => {
      mockRequest.query = {
        page: 'NaN'
      };
      addQueryOptions(queryFields)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
        code: ApiErrorCodes.QUERY_OPTIONS_PAGE,
      }));
    });
  });

});