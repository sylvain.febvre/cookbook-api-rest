import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { RequestHandler } from 'express';

export const requiredBodyParams = (...requiredParams: string[]): RequestHandler => {
  return (req, res, next) => {
    if (!requiredParams.every(p => Object.prototype.hasOwnProperty.call(req.body, p)))
      return next(getApiError(ApiErrorCodes.REQUIRED_BODY_PARAMS, { params: requiredParams }));
    return next();
  };
};