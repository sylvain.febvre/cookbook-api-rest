import { QueryAcceptedFields } from '@api/utils';
import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { RequestHandler } from 'express-serve-static-core';
import { Order } from 'sequelize';

/**
 * Middleware to check fields
 * @param options list of accepted fields for each option
 * @returns the middleware
 */
export const addQueryOptions = (options: QueryAcceptedFields): RequestHandler => {
  return (req, res, next) => {
    // Check selected fields
    if (options.show) {
      const show = options.show;
      if (req.query.fields) {
        const queryFields = req.query.fields.toString().split(',');

        if (!queryFields.every(f => show.includes(f))) {
          return next(getApiError(ApiErrorCodes.QUERY_OPTIONS_FIELDS, { acceptedFields: show }));
        }

        // format query to remove duplicate if exist
        req.api.findOptions.attributes = [...new Set(queryFields)];
      }
    }

    // Check include
    if (options.include && req.query.include) {
      const include = options.include;
      const queryInclude = req.query.include.toString().split(',');
      if (!queryInclude.every(f => include.includes(f))) {
        return next(getApiError(ApiErrorCodes.QUERY_OPTIONS_INCLUDE, { acceptedInclude: include }));
      }

      // format query to remove duplicate if exist
      req.api.findOptions.include = [...new Set(queryInclude)];
    }

    // Check sort
    if (options.sort && req.query.sort) {
      const sort = options.sort;
      const querySort = req.query.sort.toString().split(',').map(sort => sort.split('.')); // [field, order][]
      if (!querySort.every(f => sort.includes(f[0]))) {
        return next(getApiError(ApiErrorCodes.QUERY_OPTIONS_SORT, { acceptedFields: sort }));
      }
      if (req.api.findOptions.attributes && !querySort.every(f => (req.api.findOptions.attributes as string[]).includes(f[0]))) {
        return next(getApiError(ApiErrorCodes.QUERY_OPTIONS_SORT_SELECTED, { acceptedFields: req.api.findOptions.attributes }));
      }

      // format sort order 
      querySort.map(sort => {
        if (!sort[1] || sort[1] !== 'desc')
          sort[1] = 'asc';
      });

      req.api.findOptions.order = querySort as Order;
    }

    // Check limit
    if (req.query.limit) {
      const limit = parseInt(req.query.limit.toString());
      if (isNaN(limit))
        return next(getApiError(ApiErrorCodes.QUERY_OPTIONS_LIMIT));

      req.api.findOptions.limit = limit;
    }

    // Check page
    if (req.query.page) {
      const page = parseInt(req.query.page.toString());
      if (isNaN(page))
        return next(getApiError(ApiErrorCodes.QUERY_OPTIONS_PAGE));

      req.api.findOptions.offset = (page - 1) * (req.api.findOptions.limit as number);
    }

    return next();
  };
};