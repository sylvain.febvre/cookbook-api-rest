import { Request, Response, NextFunction } from 'express';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { notImplemented } from '.';

describe('notImplemented middleware', () => {
  it('should send NOT_IMPLEMENTED error', () => {
    const mockRequest: Partial<Request> = {};
    const mockResponse: Partial<Response> = {};
    const nextFunction: NextFunction = jest.fn();
    notImplemented(mockRequest as Request, mockResponse as Response, nextFunction);
    expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
      code: ApiErrorCodes.NOT_IMPLEMENTED,
    }));
  });
});