import { logReq } from '@logger';
import { RequestHandler } from 'express';
import onFinished from 'on-finished';

export const logRequest: RequestHandler = (req, res, next) => {
  onFinished(res, (err, res) => {
    const user = (req.user) ? `(User : ${req.user.id} - ${req.user.username})` : `(Not authenticated)`;
    const message = `${res.statusCode} - ${req.method} : ${req.originalUrl} from ${req.ip} - ${res.statusMessage} ${user}`;
    (res.statusCode >= 400) ? logReq.error(message) : logReq.info(message);
  });
  return next();
};