import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { RequestHandler } from 'express';

export const notFound: RequestHandler = (req, res, next) => {
  return next(getApiError(ApiErrorCodes.NOT_FOUND, { route: new URL(req.originalUrl, `${req.protocol}://${req.hostname}`).pathname }));
};