import { checkAuth, checkRole } from './auth';
import { sign } from 'jsonwebtoken';
import { JWTPayload } from '@api/utils';
import { Request, Response } from 'express';
import { ApiError, ApiErrorCodes } from '@api/utils/error';
import { JWT, User } from '@database/models';
import { createHmac } from 'crypto';

describe('auth middleware', () => {
  const payload: JWTPayload = {
    id: 2,
    role: 99,
    username: 'user',
  };

  const mockRequest: Partial<Request> = {};
  const mockResponse: Partial<Response> = {};
  const nextFunction = jest.fn();

  beforeEach(() => {
    nextFunction.mockRestore();
  });

  describe('checkAuth', () => {
    let token: string;
    let userResponse: Partial<User>;

    beforeEach(() => {
      token = sign(payload, process.env.JWT_SECRET, {
        expiresIn: '1d',
      });
      userResponse = {
        id: 2,
        username: 'user',
        role_id: 99,
        JWTs: [
          {
            id: 4,
            hash: createHmac('sha512', 'token').update(token).digest('hex'),
            user_id: 2,
          } as JWT
        ]
      };
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      mockRequest.get = () => `Bearer ${token}` as any;
      User['findByPk'] = jest.fn().mockResolvedValueOnce(userResponse);
    });

    it('should call next without error and set req.user', async () => {
      await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toBeCalledWith();
      expect(mockRequest.user).toMatchObject(payload);
    });

    describe('UNAUTHORIZED - 401', () => {
      test('authorization header is missing', async () => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        mockRequest.get = () => undefined as any;
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.AUTH_NO_HEADER,
        }));
      });

      test('header format is wrong', async () => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        mockRequest.get = () => `Wrong ${token}` as any;
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.AUTH_WRONG_HEADER,
        }));
      });

      test('no user associated in db', async () => {
        User['findByPk'] = jest.fn().mockResolvedValueOnce(undefined);
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.AUTH_USER_NOT_FOUND,
        }));
      });

      test('token invalidated', async () => {
        User['findByPk'] = jest.fn().mockResolvedValueOnce({
          id: 2,
          username: 'user1',
        } as Partial<User>);
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.AUTH_TOKEN_INVALID,
        }));
      });

      test('token fail to verify', async () => {
        JWT['destroy'] = jest.fn();
        // wrong signature
        token = sign(payload, 'Wrong Key', {
          expiresIn: '1d',
        });
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toHaveBeenNthCalledWith(1, expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.AUTH_INVALID_SIGN,
        }));
        // expired
        token = sign(payload, process.env.JWT_SECRET, { expiresIn: '-1' });
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toHaveBeenNthCalledWith(2, expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.AUTH_TOKEN_EXPIRED,
        }));
        expect(nextFunction).toBeCalledTimes(2);
      });
    });

    describe('INTERNAL_SERVER_ERROR - 500', () => {
      test('unexpected error', async () => {
        const error = new Error('Unexpected error');
        User['findByPk'] = jest.fn().mockRejectedValue(error);
        await checkAuth(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(error);
      });
    });
  });

  describe('checkRole', () => {
    beforeEach(() => {
      mockRequest.user = undefined;
      payload.role = 99;
    });

    it('should call next without error', () => {
      mockRequest.user = payload;
      checkRole(99, 1)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toHaveBeenNthCalledWith(1);
      payload.role = 0;
      checkRole(99, 1)(mockRequest as Request, mockResponse as Response, nextFunction);
      expect(nextFunction).toHaveBeenNthCalledWith(2);
      expect(nextFunction).toBeCalledTimes(2);
    });

    describe('UNAUTHORIZED - 401', () => {
      test('payload is not present', () => {
        checkRole(99)(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.NOT_SIGNED,
        }));
      });
    });

    describe('FORBIDDEN - 403', () => {
      test('wrong role and not admin', () => {
        mockRequest.user = payload;
        checkRole(2)(mockRequest as Request, mockResponse as Response, nextFunction);
        expect(nextFunction).toBeCalledWith(expect.objectContaining<Partial<ApiError>>({
          code: ApiErrorCodes.FORBIDDEN,
        }));
      });
    });
  });
});