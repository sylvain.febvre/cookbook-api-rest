import { ApiErrorCodes, getApiError } from '@api/utils/error';
import { RequestParamHandler } from 'express';
import { Model, ModelStatic } from 'sequelize';

export const checkIfExists = (model: ModelStatic<Model>): RequestParamHandler => {
  return async (req, res, next, value, param) => {
    try {

      const attributes = ['id'];
      // Number
      const id = parseInt(value, 10);
      if (isNaN(id) || id < 0)
        return next(getApiError(ApiErrorCodes.CHECK_EXISTS_WRONG_ID, { [param]: value }));

      const ressource = await model.findByPk(id, { attributes });

      // Exists
      if (!ressource)
        return next(getApiError(ApiErrorCodes.CHECK_EXISTS_NOT_FOUND, {
          ressource: model.name,
          route: new URL(req.originalUrl, `${req.protocol}://${req.hostname}`).pathname,
        }));

      return next();
    } catch (e) {
      return next(e);
    }
  };
}; 