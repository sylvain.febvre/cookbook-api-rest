import { ApiError, ApiErrorCodes, ApiStatusCode } from '.';
import { ApiErrorDetails, IApiError } from './api-error.interface';

const ERRORS_LIST: ApiError[] = [
  // GENERIC ERRORS
  {
    status: ApiStatusCode.INTERNAL_SERVER_ERROR,
    code: ApiErrorCodes.SERVER,
    name: 'INTERNAL_SERVER_ERROR',
    message: 'A server error occured',
  }, {
    status: ApiStatusCode.INTERNAL_SERVER_ERROR,
    code: ApiErrorCodes.DATABASE,
    name: 'INTERNAL_SERVER_ERROR',
    message: 'A database error occured',
  }, {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.NOT_SIGNED,
    name: 'NOT_SIGNED',
    message: 'You must be signed to access this ressource',
  }, {
    status: ApiStatusCode.FORBIDDEN,
    code: ApiErrorCodes.FORBIDDEN,
    name: 'FORBIDDEN',
    message: `You don't have permission to access this ressource`,
  }, {
    status: ApiStatusCode.FORBIDDEN,
    code: ApiErrorCodes.FORBIDDEN_UPDATE,
    name: 'FORBIDEN_UPDATE',
    message: `You don't have permission to update this ressource`,
  }, {
    status: ApiStatusCode.FORBIDDEN,
    code: ApiErrorCodes.FORBIDDEN_DELETE,
    name: 'FORBIDDEN_DELETE',
    message: `You don't have permission to detele this ressource`,
  }, {
    status: ApiStatusCode.NOT_FOUND,
    code: ApiErrorCodes.NOT_FOUND,
    name: 'NOT_FOUND',
    message: 'Route not found',
  },

  // ==============
  //   RESSOURCES
  // ==============
  // User
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.USER_NAME_TOO_SHORT,
    name: 'USER_NAME_TOO_SHORT',
    message: `Username must be at least 3 characters`,
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.USER_PASSWORD_TOO_SHORT,
    name: 'USER_PASSWORD_TOO_SHORT',
    message: 'Password must be at least 8 characters',
  }, {
    status: ApiStatusCode.CONFLICT,
    code: ApiErrorCodes.USER_NAME_ALREADY_EXISTS,
    name: 'USER_NAME8ALREADY_EXISTS',
    message: 'Username already exists',
  }, {

    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.USER_NOT_FOUND,
    name: 'USER_NOT_FOUND',
    message: 'User not found',
  }, {

    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.USER_WRONG_PASSWORD,
    name: 'USER_WRONG_PASSWORD',
    message: 'Wrong password',
  },

  // Role
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.ROLE_NAME_TOO_LONG,
    name: 'ROLE_NAME_TOO_LONG',
    message: 'Name must be 20 characters max',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.ROLE_DESC_TOO_LONG,
    name: 'ROLE_DESC_TOO_LONG',
    message: 'Description must be 255 characters max',
  }, {
    status: ApiStatusCode.CONFLICT,
    code: ApiErrorCodes.ROLE_ALREADY_EXISTS,
    name: 'ROLE_ALREADY_EXISTS',
    message: 'Role already exists',
  },

  // Recipe
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_NAME_TOO_LONG,
    name: 'RECIPE_NAME_TOO_LONG',
    message: 'Name must be 64 characters max',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_DIFFICULTY,
    name: 'RECIPE_WRONG_DIFFICULTY',
    message: 'Difficulty must be 1, 2 or 3',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_PREP_TIME,
    name: 'RECIPE_WRONG_PREP_TIME',
    message: 'Prepartion time must be a positive number (in minutes)',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_REST_TIME,
    name: 'RECIPE_WRONG_REST_TIME',
    message: 'Rest time must be a positive number (in minutes)',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_COOK_TIME,
    name: 'RECIPE_WRONG_COOk_TIME',
    message: 'Cook time must be a positive number (in minutes)',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_DESC_TOO_LONG,
    name: 'RECIPE_DESC_TOO_LONG',
    message: 'Description must be 255 characters max',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_NOTE_TOO_LONG,
    name: 'RECIPE_NAME_TOO_LONG',
    message: 'Note must be 255 characters max',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_PERSONS,
    name: 'RECIPE_WRONG_PERSONS',
    message: 'Persons must be a positive number',
  }, {
    status: ApiStatusCode.NOT_FOUND,
    code: ApiErrorCodes.RECIPE_INGREDIENT_NOT_IN,
    name: 'RECIPE_INGREDIENT_NOT_IN',
    message: 'Ingredient is not present in recipe',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_UNIT_ID,
    name: 'RECIPE_WRONG_UNIT_ID',
    message: 'Unit id must be a positive number',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.RECIPE_WRONG_QUANTITY,
    name: 'RECIPE_WRONG_QUANTITY',
    message: 'Quantity must be a positive number',
  },

  // Step
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.STEP_WRONG_RECIPE_ID,
    name: 'STEP_WRONG_RECIPE_ID',
    message: 'Recipe id must be a number',
  }, {
    status: ApiStatusCode.NOT_FOUND,
    code: ApiErrorCodes.STEP_RECIPE_NOT_FOUND,
    name: 'STEP_RECIPE_NOT_FOUND',
    message: 'Recipe does not exists',
  }, {
    status: ApiStatusCode.FORBIDDEN,
    code: ApiErrorCodes.STEP_FORBIDDEN_CREATE,
    name: 'STEP_FORBIDDEN_CREATE',
    message: `You don't have permission to create a step for this recipe`,
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.STEP_WRONG_ORDER,
    name: 'STEP_WRONG_ORDER',
    message: 'Order must be a positive number',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.STEP_INSTRUCTION_TOO_LONG,
    name: 'STEP_INSTRUCTION_TOO_LONG',
    message: 'Instruction must be less than 512 characters',
  },

  // Ingredients
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.INGREDIENT_NAME_TOO_LONG,
    name: 'INGREDIENT_NAME_TOO_LONG',
    message: 'Name must be 20 characters max',
  },

  // ===============
  //   MIDDLEWARES
  // ===============
  // Auth
  {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.AUTH_NO_HEADER,
    name: 'AUTH_NO_HEADER',
    message: 'Missing authorization header',
  }, {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.AUTH_WRONG_HEADER,
    name: 'AUTH_WRONG_HEADER',
    message: 'Wrong authorization header format, must be "Bearer $token"',
  }, {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.AUTH_USER_NOT_FOUND,
    name: 'AUTH_USER_NOT_FOUND',
    message: 'No user in database',
  }, {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.AUTH_TOKEN_INVALID,
    name: 'AUTH_TOKEN_INVALID',
    message: 'Token has been invalidated, please login again',
  }, {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.AUTH_TOKEN_EXPIRED,
    name: 'AUTH_TOKEN_EXPIRED',
    message: 'Token expired',
  }, {
    status: ApiStatusCode.UNAUTHORIZED,
    code: ApiErrorCodes.AUTH_INVALID_SIGN,
    name: 'AUTH_INVALID_SIGN',
    message: 'Invalid token signature',
  },

  // Check if exists
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.CHECK_EXISTS_WRONG_ID,
    name: 'CHECK_EXISTS_WRONG_ID',
    message: 'Id must be a positive number',
  }, {
    status: ApiStatusCode.NOT_FOUND,
    code: ApiErrorCodes.CHECK_EXISTS_NOT_FOUND,
    name: 'CHECK_EXISTS_NOT_FOUND',
    message: 'Ressource not found',
  },

  // Not implemented
  {
    status: ApiStatusCode.NOT_IMPLEMENTED,
    code: ApiErrorCodes.NOT_IMPLEMENTED,
    name: 'NOT_IMPLEMENTED',
    message: 'Route not implemented yet',
  },

  // Query options
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.QUERY_OPTIONS_FIELDS,
    name: 'QUERY_OPTIONS_FIELDS',
    message: 'Only certains fields are accepted',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.QUERY_OPTIONS_INCLUDE,
    name: 'QUERY_OPTIONS_INCLUDE',
    message: 'Only certains relations are accepted in include',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.QUERY_OPTIONS_SORT,
    name: 'QUERY_OPTIONS_SORT',
    message: 'Only certains fields are accepted in sort',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.QUERY_OPTIONS_SORT_SELECTED,
    name: 'QUERY_OPTIONS_SORT_SELECTED',
    message: 'Can sort only on selected fields',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.QUERY_OPTIONS_LIMIT,
    name: 'QUERY_OPTIONS_LIMIT',
    message: 'Limit option must be a number',
  }, {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.QUERY_OPTIONS_PAGE,
    name: 'QUERY_OPTIONS_PAGE',
    message: 'Page option must be a number'
  },

  // Required body params
  {
    status: ApiStatusCode.BAD_REQUEST,
    code: ApiErrorCodes.REQUIRED_BODY_PARAMS,
    name: 'REQ_BODY_PARAMS',
    message: 'Body missing parameters',
  }
];

const MAP_ERROR_MSG = new Map<ApiErrorCodes, IApiError>();

ERRORS_LIST.forEach(err => MAP_ERROR_MSG.set(err.code, err));

const unknownError: IApiError = {
  status: ApiStatusCode.INTERNAL_SERVER_ERROR,
  code: 0,
  name: 'UNKNONW',
  message: 'An unknonw error occured'
};

export function getApiError<T = ApiErrorDetails> (errorCode: ApiErrorCodes, details?: T): ApiError<T> {
  const {status, code, name, message} = MAP_ERROR_MSG.get(errorCode) ?? unknownError;
  return new ApiError<T>(status, code, name, message, details);
}