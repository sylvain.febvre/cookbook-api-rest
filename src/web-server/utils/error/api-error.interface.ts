import { ApiErrorCodes, ApiStatusCode } from '.';

export class ApiError<T = ApiErrorDetails> extends Error {
  constructor(
    readonly status: ApiStatusCode,
    readonly code: ApiErrorCodes,
    readonly name: string,
    readonly message: string,
    public details?: T,
  ) {
    super(message);
  }
}

export interface IApiError {
  status: ApiStatusCode;
  code: ApiErrorCodes;
  name: string;
  message: string;
}

export type ApiErrorDetails = {[key: string]: string | number | Array<string> | Array<number>};