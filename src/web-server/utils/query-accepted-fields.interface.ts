
/**
 * Represent the value accepted for each option in the query
 */
export interface QueryAcceptedFields {
  /**
   * Which fields we are allowed to select in the result
   */
  show?: string[];
  /**
   * Which relation we are allowed to add to the result
   */
  include?: string[];
  /**
   * Which fields we are allowed to sort on
   */
  sort?: string[];
}