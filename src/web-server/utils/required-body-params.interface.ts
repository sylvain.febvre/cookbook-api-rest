/**
 * != properties of the request body
 */

// User
export interface UserBody {
  username: string;
  password: string;
}

export interface UserUpdateBody extends Partial<UserBody> {
  role_id?: number;
  newPassword?: string;
}

// Role
export interface RoleBody {
  id: number;
  name: string;
  desc: string;
}

export type RoleUpdateBody = Partial<Pick<RoleBody, 'desc' | 'name'>>;

// Recipe
export interface RecipeBody {
  name: string;
  desc?: string;
  difficulty: number;
  prep_time: number;
  rest_time?: number;
  cook_time: number;
  persons?: number;
  note?: string;
}

export type RecipeUpdateBody = Partial<RecipeBody>;

// Ingredient
export interface IngredientBody {
  name: string;
}

export type IngredientUpdateBody = Partial<IngredientBody>;

// Step
export interface StepBody {
  recipe_id: number;
  order?: number;
  instruction: string;
}

export type StepUpdateBody = Partial<Pick<StepBody, 'instruction' | 'order'>>;