export interface JWTPayload extends UserPayload {
  exp?: number;
}

export interface UserPayload {
  id: number;
  role: number;
  username: string;
}