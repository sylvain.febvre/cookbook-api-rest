export interface HATEOAS {
  rel: string;
  href: string;
  action: 'GET' | 'POST' | 'PUT' | 'DELETE';
  types: 'application/json' | 'application/x-www-form-urlencoded' [];
}