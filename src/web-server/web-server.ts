import express, { RequestHandler } from 'express';
import http from 'http';
import cors from 'cors';
import { logApp } from '@logger';
import { errorHandler, logRequest, notFound } from '@api/middlewares';
import { DefaultFindOptions, sequelize } from '@database';
import { Recipe, Role, Step, User, Ingredient, Quantity, QuantityUnit } from '@database/models';
import { routes } from './routes';
import { createHmac, randomBytes } from 'crypto';

import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './docs/openapi.json';

export class WebServer {
  readonly express;
  private httpServer;

  constructor() {
    this.express = express();
    this.httpServer = http.createServer(this.express);
  }

  initialize(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.express.set('trust proxy', 'loopback, 192.168.0.254');
      this.express.use(cors());

      this.express.use('', swaggerUi.serve);
      this.express.get('', swaggerUi.setup(swaggerDocument));

      this.express.use(express.json());
      this.express.use(express.urlencoded({ extended: true }));

      // Init request with customs properties & default findOptions
      this.express.use((req, res, next) => {
        req.api = {
          findOptions: { ...DefaultFindOptions },
        };
        return next();
      });

      this.express.use(logRequest);

      this.express.get('/ping', pingController);

      // Reset the database in dev
      if (process.env.NODE_ENV === 'dev') {
        this.express.get('/sync', async (req, res, next) => {
          try {
            const force = (parseInt(req.query.force as string) === 1);

            await sequelize.sync({ force });

            if (force) {
              await Role.create({
                name: 'Admin',
                desc: 'Administrateur',
                id: 0,
              });

              await Role.create({
                name: 'User',
                desc: 'Utilisateur',
                id: 99,
              });

              // create admin
              const salt = randomBytes(Math.ceil(12 / 2)).toString('hex').slice(0, 12);
              const pwdHash = createHmac('sha512', salt).update('admin123').digest('hex');

              const newUser = await User.create({
                username: 'admin',
                password: pwdHash,
                salt,
                role_id: 0,
              });

              // create 3 quantity unit
              const qunit1 = await QuantityUnit.create({
                name: 'centiliter',
                abbreviation: 'cl',
              });
              const qunit2 = await QuantityUnit.create({
                name: 'gramme',
                abbreviation: 'g'
              });
              const qunit3 = await QuantityUnit.create({
                name: 'unit',
                abbreviation: '',
              });

              // create 3 ingredients
              const ingredient1 = await Ingredient.create({
                name: 'Farine',
              });
              const ingredient2 = await Ingredient.create({
                name: 'sucre',
              });
              const ingredient3 = await Ingredient.create({
                name: 'lait'
              });

              // create a recipe
              const recipe = await Recipe.create({
                cook_time: 20,
                difficulty: 1,
                name: 'Test recipe',
                prep_time: 15,
                user_id: newUser.id
              });

              await Quantity.bulkCreate([{
                ingredient_id: ingredient1.id,
                recipe_id: recipe.id,
                unit_id: qunit2.id,
                quantity: 100,
              }, {
                ingredient_id: ingredient2.id,
                recipe_id: recipe.id,
                unit_id: qunit3.id,
                quantity: 150,
              }, {
                ingredient_id: ingredient3.id,
                recipe_id: recipe.id,
                unit_id: qunit1.id,
                quantity: 20,
              }]);

              // create a step
              await Step.create({
                recipe_id: recipe.id,
                order: 1,
                instruction: 'Vous devez faire ça'
              });
            }

            return res.send(`Database synced successfully - force: ${force}`);
          } catch (e) {
            return next(e);
          }
        });
      }

      this.express.use('/', routes);

      this.express.use(notFound);

      this.express.use(errorHandler);

      const port = process.env.PORT || 8096;
      this.httpServer.listen(port)
        .on('listening', () => {
          logApp.info(`Server is running on \x1b[36mhttp://localhost:${port}\x1b[0m`);
          resolve(0);
        })
        .on('error', (err) => reject(err));
    });
  }

  close(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.httpServer.close((err) => err ? reject(err) : resolve(0));
    });
  }
}

export const pingController: RequestHandler = async (req, res, next) => {
  try {
    await sequelize.authenticate();
    res.json({
      webServer: 'Ok !',
      sequelize: 'Ok !',
    });
  } catch (e) {
    next(e);
  }
};