import { FindOptions, Sequelize } from 'sequelize';
import { Role, User, Recipe, Quantity, Ingredient, Step, QuantityUnit } from './models';
import { IngredientColumnsDef } from './models/ingredient.model';
import { RecipeColumnsDef } from './models/recipe.model';
import { QuantityColumnsDef } from './models/quantity.model';
import { RoleColumnsDef } from './models/role.model';
import { UserColumnsDef } from './models/user.model';
import { StepColumnsDef } from './models/step.model';
import { JWT, JWTColumnsDef } from './models/jwt.model';
import { QuantityUnitColumnsDef } from './models/ref-quantity-unit';

const env = process.env;

export const sequelize = new Sequelize({
  host: env.DB_HOST,
  database: env.DB_DATABASE,
  port: parseInt(env.DB_PORT),
  username: env.DB_USER,
  password: env.DB_PASS,
  dialect: 'postgres',
  pool: {
    max: 20,
  },
  define: {
    schema: env.DB_SCHEMA,
    defaultScope: {
      attributes: {
        exclude: ['createdAt', 'updatedAt']
      },
    }
  },
  logging: env.NODE_ENV === 'dev' ? console.log : false,
});

// Default options used when getting a ressource
export const DefaultFindOptions: FindOptions = {
  order: ['id'], // sort by id, asc
  limit: 20, // 20 result per page
  offset: 0 // first page
};

// Doing init here so when we import sequelize for the first time,
// all models are initialized

// Role
Role.init(RoleColumnsDef, {
  sequelize,
  tableName: 'role',
});

// User
User.init(UserColumnsDef, {
  sequelize,
  tableName: 'user',
  defaultScope: {
    attributes: {
      exclude: ['password', 'salt', 'role_id']
    },
  },
});

// JWT
JWT.init(JWTColumnsDef, {
sequelize,
tableName: 'jwt',
});

// Recipe
Recipe.init(RecipeColumnsDef, {
  sequelize,
  tableName: 'recipe',
});

// Ingredient
Ingredient.init(IngredientColumnsDef, {
  sequelize,
  tableName: 'ingredient',
});

// Quantity
Quantity.init(QuantityColumnsDef, {
  sequelize,
  tableName: 'recipe_ingredient',
  defaultScope: {
    attributes: {
      exclude: ['ingredient_id', 'recipe_id', 'unit_id']
    },
    include: ['unit', 'ingredient'],
  }
});

QuantityUnit.init(QuantityUnitColumnsDef, {
  sequelize,
  tableName: 'quantity_unit',
});

// Step
Step.init(StepColumnsDef, {
  sequelize,
  tableName: 'step',
});

// Models associations
// Role <> User
User.belongsTo(Role, {
  foreignKey: 'role_id',
  as: 'role'
});

Role.hasMany(User, {
  foreignKey: 'role_id',
  as: 'users',
});

// Role <> JWT
JWT.belongsTo(User, {
  foreignKey: 'user_id',
  as: 'user',
});

User.hasMany(JWT, {
  foreignKey: 'user_id',
  as: 'JWTs',
});

// User <> Recipe
Recipe.belongsTo(User, {
  foreignKey: 'user_id',
  as: 'user',
});

User.hasMany(Recipe, {
  foreignKey: 'user_id',
  as: 'recipes',
});

// Recipe <> Ingredient through Quantity
// Using hasMany and belongsTo here instead of belongsToMany because we can't choose attributes
Recipe.hasMany(Quantity, {
  foreignKey: 'recipe_id',
  as: 'quantities',
});

Quantity.belongsTo(Recipe, {
  foreignKey: 'recipe_id',
  as: 'recipe',
});

Ingredient.hasMany(Quantity, {
  foreignKey: 'ingredient_id',
  as: 'quantities',
});

Quantity.belongsTo(Ingredient, {
  foreignKey: 'ingredient_id',
  as: 'ingredient',
});

/* QuantityUnit.hasMany(Quantity, {
  foreignKey: 'unit_id',
  as: 'quantities'
}); */

Quantity.belongsTo(QuantityUnit, {
  foreignKey: 'unit_id',
  as: 'unit',
});

// Recipe <> Step
Recipe.hasMany(Step, {
  foreignKey: 'recipe_id',
  as: 'steps',
});

Step.belongsTo(Recipe, {
  foreignKey: 'recipe_id',
  as: 'recipe'
});

