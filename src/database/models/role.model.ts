import { Association, DataTypes, HasManyGetAssociationsMixin, Model, ModelAttributes } from 'sequelize';
import { Timestamps } from './models.interface';
import { User } from './user.model';

export interface RoleAttributes {
  id: number;
  name: string;
  desc: string | null;
}

export class Role extends Model<RoleAttributes>
  implements RoleAttributes, Timestamps {

  id!: number;
  name!: string;
  desc!: string | null;

  getUsers!: HasManyGetAssociationsMixin<User>;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  readonly users?: User[];

  static associations: {
    users: Association<Role, User>;
  };
}

export const RoleColumnsDef: ModelAttributes<Role, RoleAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  desc: {
    type: DataTypes.STRING,
    allowNull: false,
  }
};