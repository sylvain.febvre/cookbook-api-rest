export interface Timestamps {
  readonly createdAt: Date;
  readonly updatedAt: Date;
}