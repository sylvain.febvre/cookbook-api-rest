import { Association, DataTypes, Model, ModelAttributes } from 'sequelize';
import { QuantityUnit } from '.';
import { Ingredient } from './ingredient.model';
import { Timestamps } from './models.interface';
import { Recipe } from './recipe.model';


export interface QuantityAttributes {
  ingredient_id: number;
  recipe_id: number;
  unit_id: number;
  quantity: number;
}

export class Quantity extends Model<QuantityAttributes>
  implements QuantityAttributes, Timestamps {

  ingredient_id!: number;
  recipe_id!: number;
  unit_id!: number;
  quantity!: number;

  readonly recipe!: Recipe;
  readonly ingredient!: Ingredient;
  readonly unit!: QuantityUnit;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  static associations: {
    recipe: Association<Quantity, Recipe>;
    ingredient: Association<Quantity, Ingredient>;
    unit: Association<Quantity, QuantityUnit>;
  }
}

export const QuantityColumnsDef: ModelAttributes<Quantity, QuantityAttributes> = {
  ingredient_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
  },
  recipe_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
  },
  unit_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  quantity: {
    type: DataTypes.INTEGER,
    allowNull: false,
  }
};