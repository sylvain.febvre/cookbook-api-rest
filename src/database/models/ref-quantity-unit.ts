import { Model, ModelAttributes, DataTypes, Optional } from 'sequelize';
import { Timestamps } from './models.interface';


export interface QuantityUnitAttributes {
  id: number;
  name: string;
  abbreviation: string;
  // desc: string,
}

export class QuantityUnit extends Model<QuantityUnitAttributes, Optional<QuantityUnitAttributes, 'id'>>
  implements QuantityUnitAttributes, Timestamps {

  id!: number;
  name!: string;
  abbreviation!: string;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  /* readonly quantities?: Quantity[];

  static associations: {
    quantities: Association<QuantityUnit, Quantity>;
  }; */
}

export const QuantityUnitColumnsDef: ModelAttributes<QuantityUnit, QuantityUnitAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  abbreviation: {
    type: DataTypes.STRING,
    allowNull: false,
  }
};