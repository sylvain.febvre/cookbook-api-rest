import { Association, BelongsToGetAssociationMixin, BelongsToSetAssociationMixin, DataTypes, HasManyGetAssociationsMixin, Model, ModelAttributes, Optional } from 'sequelize';
import { Timestamps } from './models.interface';
import { Role } from '@database/models';
import { Recipe } from './recipe.model';
import { JWT } from './jwt.model';

export interface UserAttributes {
  id: number;
  username: string;
  password: string;
  salt: string;
  role_id: number;
}

export class User extends Model<UserAttributes, Optional<UserAttributes, 'id' | 'role_id'>>
  implements UserAttributes, Timestamps {

  id!: number;
  username!: string;
  password!: string;
  salt!: string;
  role_id!: number;

  getRole!: BelongsToGetAssociationMixin<Role>;
  setRole!: BelongsToSetAssociationMixin<Role, number>;

  getRecipes!: HasManyGetAssociationsMixin<Recipe>;

  getJWTs!: HasManyGetAssociationsMixin<JWT>;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  readonly role?: Role;
  readonly recipes?: Recipe[];
  readonly JWTs?: JWT[];

  static associations: {
    role: Association<User, Role>;
    recipes: Association<User, Recipe>;
    JWTs: Association<User, JWT>;
  }
}

export const UserColumnsDef: ModelAttributes<User, UserAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  password: {
    type: DataTypes.STRING(128),
    allowNull: false,
  },
  salt: {
    type: DataTypes.STRING(12),
    allowNull: false,
  },
  username: {
    type: DataTypes.STRING(20),
    allowNull: false,
  },
  role_id: {
    type: DataTypes.INTEGER,
    defaultValue: 99,
  },
};