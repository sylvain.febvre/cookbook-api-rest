import { Model, Optional, BelongsToGetAssociationMixin, Association, ModelAttributes, DataTypes, HasManyGetAssociationsMixin } from 'sequelize';
import { Timestamps } from './models.interface';
import { Quantity } from './quantity.model';
import { Step } from './step.model';
import { User } from './user.model';

export interface RecipeAttributes {
  id: number;
  name: string;
  desc: string;
  difficulty: number;
  prep_time: number;
  rest_time: number;
  cook_time: number;
  persons: number;
  note: string;
  user_id: number;
  // img_id: number,
}

export class Recipe extends Model<RecipeAttributes, Optional<RecipeAttributes, 'id' | 'note' | 'desc' | 'persons' | 'rest_time'>>
  implements RecipeAttributes, Timestamps {

  id!: number;
  name!: string;
  desc!: string;
  difficulty!: number;
  prep_time!: number;
  rest_time!: number;
  cook_time!: number;
  persons!: number;
  note!: string;
  user_id!: number;

  getUser!: BelongsToGetAssociationMixin<User>;
  getSteps!: HasManyGetAssociationsMixin<Step>;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  readonly user?: User;
  readonly quantities?: Quantity[];
  readonly steps?: Step[];

  static associations: {
    user: Association<Recipe, User>;
    quantities: Association<Recipe, Quantity>;
    steps: Association<Recipe, Step>;
  }
}

export const RecipeColumnsDef: ModelAttributes<Recipe, RecipeAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING(64),
    allowNull: false,
  },
  desc: {
    type: DataTypes.STRING(255),
  },
  difficulty: {
    type: DataTypes.SMALLINT,
    allowNull: false,
  },
  prep_time: {
    type: DataTypes.SMALLINT,
    allowNull: false,
  },
  rest_time: {
    type: DataTypes.SMALLINT,
    defaultValue: 0,
  },
  cook_time: {
    type: DataTypes.SMALLINT,
    allowNull: false,
  },
  persons: {
    type: DataTypes.SMALLINT,
    defaultValue: 1,
  },
  note: {
    type: DataTypes.STRING(255),
  },
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
};