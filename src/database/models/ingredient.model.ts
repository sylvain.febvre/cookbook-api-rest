import { Model, Association, ModelAttributes, DataTypes, Optional } from 'sequelize';
import { Timestamps } from './models.interface';
import { Quantity } from './quantity.model';


export interface IngredientAttributes {
  id: number;
  name: string;
  // img_id: number,
  // desc: string,
}

export class Ingredient extends Model<IngredientAttributes, Optional<IngredientAttributes, 'id'>>
  implements IngredientAttributes, Timestamps {

  id!: number;
  name!: string;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  readonly quantities?: Quantity[];

  static associations: {
    quantities: Association<Ingredient, Quantity>;
  };
}

export const IngredientColumnsDef: ModelAttributes<Ingredient, IngredientAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  }
};