import { Association, BelongsToGetAssociationMixin, DataTypes, Model, ModelAttributes, Optional } from 'sequelize';
import { User } from '.';
import { Timestamps } from './models.interface';

export interface JWTAttributes {
  id: number;
  hash: string;
  user_id: number;
}


export class JWT extends Model<JWTAttributes, Optional<JWTAttributes, 'id'>>
  implements JWTAttributes, Timestamps {

  id!: number;
  hash!: string;
  user_id!: number;

  getUser!: BelongsToGetAssociationMixin<User>;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  readonly user?: User;

  static associations: {
    user: Association<JWT, User>;
  }
}

export const JWTColumnsDef: ModelAttributes<JWT, JWTAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  hash: {
    type: DataTypes.STRING(128),
    allowNull: false,
  },
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  }
};