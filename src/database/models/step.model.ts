import { Association, DataTypes, Model, ModelAttributes, Optional } from 'sequelize';
import { Timestamps } from './models.interface';
import { Recipe } from './recipe.model';


export interface StepAttributes {
  id: number;
  recipe_id: number;
  order: number;
  // img_id: number,
  instruction: string;
}

export class Step extends Model<StepAttributes, Optional<StepAttributes, 'id'>>
  implements StepAttributes, Timestamps {

  id!: number;
  recipe_id!: number;
  order!: number;
  instruction!: string;

  readonly createdAt!: Date;
  readonly updatedAt!: Date;

  readonly recipe?: Recipe;

  static associations: {
    recipe: Association<Step, Recipe>;
  }

}

export const StepColumnsDef: ModelAttributes<Step, StepAttributes> = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  recipe_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    // unique: 'recipe_step',
  },
  order: {
    type: DataTypes.INTEGER,
    allowNull: false,
    // unique: 'recipe_step',
  },
  instruction: {
    type: DataTypes.STRING(512),
    allowNull: false,
  }
};