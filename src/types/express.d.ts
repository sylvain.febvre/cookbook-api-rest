declare namespace Express {
  interface Request {
    api: {
      findOptions: import('sequelize').FindOptions;
    };
    // payload?: import('@api/utils').JWTPayload;
    user?: import('@api/utils').UserPayload;
  }
}