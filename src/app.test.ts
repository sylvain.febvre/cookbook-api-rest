import { mockProcessExit } from 'jest-mock-process';

import App from './app';
import { logApp } from '@logger';
import { WebServer } from './web-server';

jest.mock('./web-server');
jest.mock('@logger');

describe('Core application', () => {
  let mockExit: jest.SpyInstance<never, [number?]>;
  let app: App;

  beforeAll(() => {
    jest.restoreAllMocks();
    mockExit = mockProcessExit();
  });

  afterAll(() => {
    mockExit.mockRestore();
  });

  it('should run application', async () => {
    app = new App();
    const start = await app.init();
    expect(start).toBe(0);
    await app.shutdown(0);
    expect(mockExit).toBeCalledWith(0);
  });

  it('should log SIGTERM and exit with code 0', async () => {
    app = new App();
    await app.init();
    process.emit('SIGTERM', 'SIGTERM');
    expect(logApp.info).toBeCalledWith('Received SIGTERM');
    expect(mockExit).toBeCalledWith(0);
  });

  it('should log SIGINT and exit with code 0', async () => {
    app = new App();
    await app.init();
    process.emit('SIGINT', 'SIGINT');
    expect(logApp.info).toBeCalledWith('Received SIGINT');
    expect(mockExit).toBeCalledWith(0);
  });

  it('should log uncaughtException and exit with code 1', async () => {
    app = new App();
    await app.init();
    process.emit('uncaughtException', new Error());
    expect(logApp.error).toBeCalledWith('Uncaught exception');
    expect(mockExit).toBeCalledWith(1);
  });

  it('should exit with code 2', async () => {
    jest.spyOn(WebServer.prototype, 'initialize').mockImplementation(() => {
      throw new Error('Test Init Error');
    });
    app = new App();
    await app.init();
    expect(mockExit).toBeCalledWith(2);
  });

  it('should exit with code 3', async () => {
    jest.spyOn(WebServer.prototype, 'close').mockImplementation(() => {
      throw new Error('Test Close Error');
    });
    app = new App();
    await app.shutdown(0);
    expect(mockExit).toBeCalledWith(3);
  });
});