declare namespace NodeJS {
  export interface ProcessEnv {
    DB_USER: string;
    DB_PASS: string;
    DB_DATABASE: string;
    DB_HOST: string;
    DB_PORT: string;
    DB_SCHEMA: string;
    PORT: string;
    JWT_SECRET: string;
    NODE_ENV: string;
  }
}