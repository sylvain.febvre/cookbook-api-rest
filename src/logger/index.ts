import { createLogger, format } from 'winston';
const { combine, timestamp, printf } = format;

import path from 'path';
import 'winston-daily-rotate-file';
import { Console, DailyRotateFile } from 'winston/lib/winston/transports';

process.env.DIRLOG = process.env.DIRLOG || path.join(__dirname, '..', '..', 'logs');

const logFileFormat = combine(
  timestamp({ format: 'DD-MM-YYYY HH:mm:ss.SSS' }),
  printf(
    ({ level, message, timestamp }) => `${timestamp} - ${level}: ${message}`
  )
);

/**
 * Create app logger
 */
export const logApp = createLogger({
  transports: [
    new DailyRotateFile({
      format: logFileFormat,
      filename: path.join(
        process.env.DIRLOG,
        '%DATE%',
        `cluster_${process.env.NODE_APP_INSTANCE || 0}.error.log`
      ),
      level: 'error',
    }),
    new DailyRotateFile({
      format: logFileFormat,
      filename: path.join(
        process.env.DIRLOG,
        '%DATE%',
        `cluster_${process.env.NODE_APP_INSTANCE || 0}.log`
      ),
    })
  ]
});

/**
 * Create request logger
 */
export const logReq = createLogger({
  transports: [
    new DailyRotateFile({
      format: logFileFormat,
      filename: path.join(
        process.env.DIRLOG,
        '%DATE%',
        'request.error.log',
      ),
      level: 'error',
    }),
    new DailyRotateFile({
      format: logFileFormat,
      filename: path.join(
        process.env.DIRLOG,
        '%DATE%',
        'request.log',
      ),
    })
  ]
});

/**
 * Add console log in dev
 */
if (process.env.NODE_ENV === 'dev') {
  const c = new Console({
    format: format.simple(),
  });
  logApp.add(c);
  logReq.add(c);
  logApp.info(`Logs location : ${process.env.DIRLOG}`);
}