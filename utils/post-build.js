/**
 * Script to convert ts alias path to relative path after the typescript build
 */

const path = require('path');
const fs = require('fs');
const { replaceInFileSync } = require('replace-in-file');

const buildConfig = require('../tsconfig.build.json');

const pathAlias = require('../tsconfig.base.json').compilerOptions.paths;

const baseDir = path.join(__dirname, '..');

const outDir = path.join(baseDir, buildConfig.compilerOptions.outDir);

const readdirRec = (dir) => {
  fs.readdir(dir, { withFileTypes: true }, (err, list) => {
    if (err)
      return console.error(err);
    list.forEach(file => {
      const filePath = path.join(dir, file.name);
      if (file.isDirectory()) {
        readdirRec(filePath);
      } else {
        fs.readFile(filePath, (err, data) => {
          if (err)
            return console.error(err);
          console.log('file : ' + filePath);
          for (const key in pathAlias) {
            if (Object.hasOwnProperty.call(pathAlias, key)) {

              const element = pathAlias[key][0];
              const regexp = new RegExp(key);
              const result = data.toString().match(regexp);
              if (result) {
                const relativePath = './' + path.relative(dir, path.join(outDir, element)).replace(/\\/g, '/');
                console.log('  ' + result[0] + ' => ' + relativePath);
                const res = replaceInFileSync({
                  files: filePath,
                  from: regexp,
                  to: relativePath,
                });
              }
            }
          }
        })
      }
    });
  })
}

fs.copyFileSync((path.join(baseDir, 'package.json')), path.join(outDir, 'package.json'));

readdirRec(outDir);